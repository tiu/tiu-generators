# frozen_string_literal: true

require 'helpers/generator_helper'

module Tiu
  module Generators # :nodoc:
    class SearchFormGenerator < Rails::Generators::NamedBase # :nodoc:
      include ::Helpers::GeneratorHelper
      include Rails::Generators::ResourceHelpers

      source_root File.expand_path('templates', __dir__)

      argument :attributes, type: :array, banner: 'field:type field:type'

      def create_root_folder
        empty_directory 'app/forms'
      end

      def copy_form_object
        template 'search_form.rb.erb', File.join('app/forms', "#{name}_search_form.rb")
      end

      def update_controller_index_action
        standard_resource_loading = /    @#{plural_table_name} = #{class_name}\..*\n/
        insert_into_file controller_file,
                  "    @search = #{form_class_name}.new(#{params_method_name})\n" \
                  "    @#{plural_table_name} = @search.apply(#{plural_table_name}: @#{plural_table_name})\n",
                  after: standard_resource_loading
      end

      def update_controller_parameter_list
        return if File.readlines(controller_file).any? { |line| line["def #{params_method_name}"] } # already exists
        insert_into_file(
          controller_file,
          "\n" \
          "    def #{params_method_name}\n" \
          "      params[:q]&.permit(#{attributes.map { |a| ":#{a.name}" }.join(', ')})\n" \
          "    end\n" \
          "    helper_method :#{params_method_name}\n",
          after: "private\n"
        )
      end

      def copy_form_partial
        template '_search_form.html.erb', File.join('app/views', name.pluralize, '_search_form.html.erb')
      end

      def update_index_page
        index_view_path = File.join('app/views', controller_file_path, 'index.html.erb')
        prepend_to_file index_view_path, "<%= render 'search_form', search: @search %>\n"
      end

      private

      def form_class_name
        "#{class_name}SearchForm"
      end

      def params_method_name
        "#{name}_search_form_params"
      end

      def controller_file
        File.join('app/controllers', "#{controller_file_path}_controller.rb")
      end
    end
  end
end
