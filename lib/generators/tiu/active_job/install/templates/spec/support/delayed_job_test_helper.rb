# Use DelayedJob queue adapter for tests with :delayed_job metadata.
#
# NOTE: Not using the :test adapter also means *not* using any ActiveJob::TestHelper methods in the tests.
# (e.g. perform_enqueued_jobs, have_been_performed, etc.)
#
# Reference:
# - [Job spec](https://relishapp.com/rspec/rspec-rails/docs/job-specs/job-spec)
# - [around hooks](https://relishapp.com/rspec/rspec-core/v/3-10/docs/hooks/around-hooks)
# - [filter](https://relishapp.com/rspec/rspec-core/docs/hooks/filters)
# - [User-defined metadata](https://relishapp.com/rspec/rspec-core/docs/metadata/user-defined-metadata)
#
# NOTE: Rails 6.0 inconsistently overrides ActiveJob.queue_adapter (https://github.com/rails/rails/issues/37270).
# This issue means it is tricky to switch queue adapters.
# This around hook is derived from https://github.com/rails/rails/issues/37270#issuecomment-553927324
RSpec.configure do |config|
  config.around(:each, delayed_job: true) do |example|
    # NOTE: if ActiveJob::TestHelper is included, the :test adapter still gets forced.
    expect(self.class.included_modules).not_to include(ActiveJob::TestHelper)

    original_queue_adapter = ActiveJob::Base.queue_adapter
    descendants = (ActiveJob::Base.descendants + [ActiveJob::Base]).select do |klass|
      klass.singleton_class.public_instance_methods(false).include?(:_queue_adapter)
    end

    Delayed::Worker.delay_jobs = false

    # Override the default :test queue adapter with the :delayed_job adapter.
    ActiveJob::Base.queue_adapter = :delayed_job
    descendants.each(&:disable_test_adapter)

    example.run

    # Restore the :test queue adapter
    descendants.each { |a| a.enable_test_adapter(ActiveJob::QueueAdapters::TestAdapter.new) }
    ActiveJob::Base.queue_adapter = original_queue_adapter
  end
end
