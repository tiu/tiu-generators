# Extensions for usability and readable code.
#
# TODO: (enhancement ideas)
# * https://www.sitepoint.com/delayed-jobs-best-practices/
# * https://robots.thoughtbot.com/action-mailer-and-active-job-sitting-in-a-tree
# * https://www.sitepoint.com/dont-get-activejob/
module DelayedJobExtensions

  def self.prepended(base)
    base.class_eval do
      scope :sorted, -> { by_priority } # matches Delayed::Job.reserve, which uses :by_priority

      # See (https://github.com/ejschmitt/delayed_job_web/blob/master/lib/delayed_job_web/application/app.rb#L142)
      scope :failed, -> { where.not(last_error: nil) }
      scope :pending, -> { where(attempts: 0, locked_at: nil) }
      scope :working, -> { where.not(locked_at: nil) }
      scope :overdue, -> { where(locked_at: nil, run_at: ...Time.current - Delayed::Worker.sleep_delay.seconds) }
      scope :troubled, -> { failed.or(overdue) }
    end

    base.extend ClassMethods
  end

  # Friendly status names
  def status
    if locked_at.present?
      :working
    elsif last_error.present?
      :failed
    elsif attempts.zero? && locked_at.nil?
      :pending
    else
      raise ArgumentError, 'Unexpected status'
    end
  end

  def job_class
    payload_object.job_data['job_class']
  end

  # Deserialized job object, matching what would be created by :perform_now.
  # Useful to get deserialized arguments.
  # Serialized arguments available directly via payload_object.job_data['arguments']
  def object
    @object ||= begin
      job_object = ActiveJob::Base.deserialize(payload_object.job_data)
      job_object.send(:deserialize_arguments_if_needed)
      job_object
    rescue ActiveJob::DeserializationError
      nil
    end
  end

  module ClassMethods
    # Use demodularized model_name so url helpers know how to route to this.
    # That is, `link_to(job, job)` will work.
    def model_name
      ActiveModel::Name.new(Delayed::Job, nil, 'Job')
    end
  end
end

module DelayedWorkerExtensions
  # Notify on all job errors
  def handle_failed_job(job, error)
    message = "[DelayedJob] #{job.job_class} [#{job.id}] failed due to #{error.class.name}: #{error.message}"
    Rails.logger.error message

    # https://github.com/smartinez87/exception_notification#background-notifications
    data = { application: I18n.t('app.title'), environment: Rails.env, message: message }
    ExceptionNotifier.notify_exception(error, data: data)

    super
  end
end

Rails.application.configure do
  config.after_initialize do
    Delayed::Job.prepend DelayedJobExtensions
    Delayed::Worker.prepend DelayedWorkerExtensions
  end
end
