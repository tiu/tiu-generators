# frozen_string_literal: true

class JobsController < ApplicationController
  before_action :require_user
  before_action :set_jobs, only: [:index]
  before_action :set_job, only: [:destroy, :update]
  load_and_authorize_resource class: "Delayed::Job"

  # GET /jobs
  def index; end

  # DELETE /jobs/1
  def destroy
    destroy_record(@job)
  end

  # PATCH/PUT /jobs/1
  def update
    flash[:notice] = "Job #{@job.to_param} should start momentarily." if @job.update(run_at: Time.current)
    redirect_back_or_to jobs_path, allow_other_host: false
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Delayed::Job.find(params[:id])
    end

    def set_jobs
      # See (https://github.com/ejschmitt/delayed_job_web/blob/master/lib/delayed_job_web/application/app.rb#L142)
      @total_job_count   = Delayed::Job.count
      @failed_job_count  = Delayed::Job.failed.count
      @pending_job_count = Delayed::Job.pending.count
      @working_job_count = Delayed::Job.working.count
      @overdue_job_count = Delayed::Job.overdue.count

      @jobs = Delayed::Job.sorted.reverse_order.page(page_params)

      # Filter by status
      @jobs = @jobs.send params[:status] if params[:status].in? %w[pending working failed overdue]
    end

end
