# frozen_string_literal: true

module JobHelper

  CSS_CLASS_FOR_STATUS = {
    'working' => 'badge-primary',   # being processed
    'pending' => 'badge-secondary', # waiting for processing
    'failed' => 'badge-danger'
  }.freeze

  def job_status_tag(status, **_args)
    # Read status when given an Delayed::Job object
    status = status.status if status.is_a? Delayed::Job
    status = status.to_s

    span_class = CSS_CLASS_FOR_STATUS[status] || 'badge-danger'

    options = { class: "badge #{span_class} job-status-tag",
                title: "#{status.humanize} Status",
                'data-toggle': 'tooltip' }

    tag.span status.humanize, **options
  end

  def link_to_argument(argument)
    if (url = try_url_for(argument))
      link_to argument, url
    else
      argument
    end
  end

  def try_url_for(model)
    route = show_route_for(model)
    return if route.nil?

    params = { only_path: true }
             .merge(route.defaults) # ex. { controller: 'users', action: 'show' }
             .merge(model.slice(*route.required_parts).symbolize_keys)

    Rails.application.routes.url_for(params)
  end

  private

    # Return first :show route defined for this model, otherwise nil.
    def show_route_for(model)
      return unless model.respond_to? :model_name

      Rails.application.routes.routes.find do |route|
        route.defaults == { controller: model.model_name.route_key, action: 'show' }
      end
    end

end
