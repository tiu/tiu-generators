# frozen_string_literal: true

# Inspired by UK Government's e-petitions service:
# * https://github.com/alphagov/e-petitions/blob/af93a3eb3e7178efe5c9f30bdf36cd3ad2167180/app/jobs/email_job.rb#L4-L18
# * https://github.com/rails/rails/issues/27298

require 'net/smtp'

class EmailJob < ApplicationJob

  PERMANENT_FAILURES = [
    Net::SMTPFatalError,
    Net::SMTPSyntaxError,
    OpenSSL::SSL::SSLError,
    Net::SMTPAuthenticationError
  ].freeze

  TEMPORARY_FAILURES = [
    Net::OpenTimeout,
    Net::SMTPServerBusy,
    Errno::ECONNRESET,
    Errno::ECONNREFUSED,
    Errno::ETIMEDOUT,
    Timeout::Error,
    EOFError
  ].freeze

  queue_as :high_priority

  # Send notification for any permanent failures.
  rescue_from *PERMANENT_FAILURES do |exception|
    LoggingUtility.notify(exception, message: "#{self.class.name} had a permanent failure.")
    raise exception # propagate the failure
  end

  # Retry job on any temporary issues, such as a network timeout.
  rescue_from *TEMPORARY_FAILURES do |exception|
    LoggingUtility.log(exception, message: "#{self.class.name} had a temporary failure.")
    raise exception # propagate the failure
  end

  # ex. `SomeMailer.some_email(*args).deliver_now`
  def perform(*_args)
    raise NotImplementedError, "#{self.class.name}.perform is an abstract method."
  end
end
