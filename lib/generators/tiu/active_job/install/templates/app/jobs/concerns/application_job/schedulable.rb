class ApplicationJob
  module Schedulable
    def self.included(base)
      base.before_enqueue :schedule_unscheduled_jobs
      base.before_enqueue :prevent_stale_enqueue
    end

    # Nearest Date on the schedule that hasn't passed yet. Will be today when the job is performed.
    def closest_reminder_on
      @closest_reminder_on ||= begin
        schedule_seek_to_closest
        schedule.next
      end
    end

    # The nearest future Date on the schedule. This is when the next job is scheduled for.
    def next_reminder_on
      @next_reminder_on ||= closest_reminder_on.future? ? closest_reminder_on : schedule.next
    end

    private

      # Returns an enumeration that yields each Date/Time on which the job should run.
      def schedule
        raise NotImplementedError
      end

      # Advance the schedule to today. If today isn't found, the next upcoming day.
      def schedule_seek_to_closest
        schedule.rewind
        schedule.next until schedule.peek >= Date.current
      end

      def every_day
        Enumerator.produce(1.day) { |prev| prev + 1.day }
      end

      def every_week
        Enumerator.produce(1.week) { |prev| prev + 1.week }
      end

      def every_month
        Enumerator.produce(1.month) { |prev| prev + 1.month }
      end

      # :before_enqueue Callback handler
      # Enqueue according to the :schedule when none is provided.
      def schedule_unscheduled_jobs
        self.scheduled_at ||= closest_reminder_on.in_time_zone.to_f
      end
  end
end
