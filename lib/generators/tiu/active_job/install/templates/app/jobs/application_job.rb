# frozen_string_literal: true

# http://guides.rubyonrails.org/active_job_basics.html
# TODO: verify retry_on, discard_on and logging from subscribe('discard.active_job')
#       https://blog.bigbinary.com/2019/07/09/rails-6-adds-hooks-to-activejob-around-retries-and-discards.html
class ApplicationJob < ActiveJob::Base
  # Automatically retry jobs that encountered a deadlock
  retry_on ActiveRecord::Deadlocked

  # Most jobs are safe to ignore if the underlying records are no longer available
  discard_on ActiveJob::DeserializationError

  # Enforce by adding this to a job class:
  #   before_enqueue :prevent_stale_enqueue
  class StaleEnqueueError < StandardError
    DEFAULT_MESSAGE = "Scheduled in the past. Perhaps there is a miscalculation in :wait or :wait_until parameters?"
    attr_reader :job

    def initialize(message = DEFAULT_MESSAGE, job: nil)
      @job = job
      message += " (Job scheduled_at: #{Time.at(job.scheduled_at).in_time_zone})" if job
      super(message)
    end
  end

  private

    # before_enqueue :prevent_stale_enqueue
    def prevent_stale_enqueue
      raise StaleEnqueueError.new(job: self) if scheduled_at < 1.minute.ago.to_f
    end
end
