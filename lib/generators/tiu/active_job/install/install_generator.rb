# frozen_string_literal: true

require 'helpers/generator_helper'

module Tiu
  module ActiveJob
    # Scaffolds ActiveJob
    class InstallGenerator < Rails::Generators::Base
      include ::Helpers::GeneratorHelper
      source_root File.expand_path('templates', __dir__)

      # Every method that is declared below will be automatically executed when the generator is run

      # Verify dependencies specific to this generator, since they are not
      # listed as dependencies in the gemspec.
      def check_dependencies
        append_to_file 'Gemfile',
        "gem 'daemons', '~> 1.3'         # Manage delayed_job background process\n" \
        "gem 'delayed_job_active_record' # Manage background jobs"

        verify_gem 'delayed_job_active_record', '> 4.0'
        verify_gem 'daemons', '~> 1.3'
      end

      def migrate
        generate 'delayed_job:active_record'
      end

      def copy_templates
        directory 'app'
        directory 'config'
        directory 'spec'
      end

      def load_spec_support_directory
        # Commented by default. Format varies some with spotty Rubocop fixes.
        uncomment_lines 'spec/rails_helper.rb', /Rails.root.join\W+spec\W+support/
      end

      # https://github.com/collectiveidea/delayed_job#active-job
      def configure_active_job
        %w[dev demo production].each do |env|
          config_file = "config/environments/#{env}.rb"
          uncomment_lines config_file, 'config.active_job.queue_adapter'
          uncomment_lines config_file, 'config.active_job.queue_name_prefix'
          gsub_file config_file, ' = :resque', ' = :delayed_job'
        end
      end

      def finish_up
        route 'resources :jobs, only: [:index, :destroy, :update]'
        append_to_file 'app/views/application/_nav.html.erb', <<~LINES
          <!-- TODO: add to admin menu: -->
          <%= link_to icon('fas', 'tasks', 'Jobs', class: 'fa-fw'), jobs_path, class: 'dropdown-item' %>

          <!-- TODO: add to navbar: -->
          <%= render 'job_nav', troubled_job_count: Delayed::Job.troubled.count %>
        LINES

        append_to_file 'config/locales/views/en.yml',
          "  jobs:\n" \
          "    index:\n" \
          "      overdue_jobs: >\n" \
          "        %{count} overdue jobs indicate a system issue. Please contact a System Administrator.\n"
      end
    end
  end
end
