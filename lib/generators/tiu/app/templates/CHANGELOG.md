# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project somewhat adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Unreleased

Improvements

* Added [Release Notes](/releases) page, available in the Admin menu.

Fixes

* Fix error when saving record.

Maintenance

* Upgrade libraries and packages.

## 1.0.0 (2020-01-15)

* Example
