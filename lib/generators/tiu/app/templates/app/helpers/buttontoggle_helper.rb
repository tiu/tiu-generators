# frozen_string_literal: true

module ButtontoggleHelper

  # Generate radio button inputs in a group that will toggle
  # For available options, @see (https://getbootstrap.com/docs/4.1/components/buttons/#checkbox-and-radio-buttons)
  #
  # Usage:
  #   radio_button_toogle_tag f, :user_id, collection, value: :id, name: :to_s, title: :humanize
  #
  # +:form+
  # +:field_name+
  # +:collection+ - enumerable collection of items. Must respond to the given methods for :name, :value, :title.
  # +:name+ - method to get the name from the collection item
  # +:value+ - method to get the value from the collection item
  # +:title+ - method to get the title from the collection item. By default, the title is looked up.
  #   Key is derived from the model, field name and field value (e.g. `users.user.status.active`)
  def radio_button_toogle_tag(form, field_name, collection, name: :first, value: :first)
    current_value = form.object[field_name].to_s

    tag.div class: 'btn-group btn-group-toggle', 'data-toggle': 'buttons' do
      collection.each do |item|
        item_name = item.send(name)
        item_value = item.send(value)
        item_title = I18n.t("#{field_name}.#{item_name}",
                            scope: [form.object.model_name.plural, form.object.model_name.singular],
                            default: nil)
        concat(
          tag.label(field_name,
                    title: item_title,
                    'data-toggle': 'tooltip',
                    class: ["btn btn-outline-secondary", ('active' if current_value.eql?(item_value))]) do
            concat form.radio_button(field_name, item_value)
            concat item_name.humanize
          end
        )
      end
    end
  end

  # Display a check or 'x' for a boolean value
  def check_box_tag(value, **options)
    icon_class = value ? 'check-circle' : 'times-circle'
    icon(:fas, icon_class, title: value, 'data-toggle': 'tooltip', **options)
  end

end
