// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import Rails from '@rails/ujs'
Rails.start()

import * as ActiveStorage from '@rails/activestorage'
ActiveStorage.start()

import 'jquery'
import 'bootstrap_and_overrides'

import 'analytics'
import 'flatpickr_setup'
import 'forms'
import 'manipulators'
import 'nested_fields'
import 'user_session'

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
