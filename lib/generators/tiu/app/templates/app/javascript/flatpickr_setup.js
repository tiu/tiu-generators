import flatpickr from 'flatpickr'

document.addEventListener('DOMContentLoaded', (event) => {
  // Overrides a few default configs for all instances.
  // - https://flatpickr.js.org/options/
  flatpickr('[data-provide="flatpickr"]', { allowInput: true, allowInvalidPreload: true })
})
