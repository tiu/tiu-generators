# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

# Development Setup

```sh
brew install ffmpeg imagemagick poppler vips # activestorage
brew install yarn # webpacker
brew install postgresql # pg
```

# Server Setup

Assumes an Ansible-provisioned web server. Refer to the [TIU Ansible Playbooks](https://bitbucket.org/tiu/ansible-playbooks).

```sh
cap production rvm:create_gemset
cap production deploy:setup
```

(create database user and database)
[Install Node.js LTS](https://nodejs.org/en/download/) from [NodeSource](https://github.com/nodesource/distributions/blob/master/README.md#deb) for Ubuntu 14.04 LTS

    curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    sudo apt-get update && sudo apt-get install nodejs

[Install yarn](https://yarnpkg.com/lang/en/docs/install/#debian-stable)

    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
    sudo apt-get update && sudo apt-get install yarn

cap production deploy

# Initialize the Database

    RAILS_ENV=production bundle exec rails db:seed
    RAILS_ENV=production bundle exec rails db:import:example

# Full-Text Search

Rebuild for existing records:

    [Address, Phone, User].each { |klass| PgSearch::Multisearch.rebuild klass }

Migrations to enable Postgres extensions will only serve to verify they're in place in production,
because the application database user isn't a superuser and cannot create/enable extensions. Prior to deploy,
enable the extensions with a superuser account:

    sudo -u postgres psql -d example_app_production -tAc "CREATE EXTENSION IF NOT EXISTS \"pg_stat_statements\""
    sudo -u postgres psql -d example_app_production -tAc "CREATE EXTENSION IF NOT EXISTS \"fuzzystrmatch\""
    sudo -u postgres psql -d example_app_production -tAc "CREATE EXTENSION IF NOT EXISTS \"unaccent\""

# Test with Production Data

Copy the production database to demo, then scramble user emails. In many apps,
you also need to carefully remove PII or PHI.

    RAILS_ENV=production bundle exec rails db:snapshot
    # update .env with demo environment DATABASE_PASSWORD, DATABASE_HOST
    cap demo rake[db:migrate,VERSION=0]
    RAILS_ENV=demo bundle exec rails db:snapshot:restore[name-from-desired-snapshot.sql]
    RAILS_ENV=demo bundle exec rails db:environment:set RAILS_ENV=demo # or just use `rails db:migrate`
    RAILS_ENV=demo bundle exec rails db:scramble_user_emails
