# frozen_string_literal: true

# == Resources
# * Bootstrap 4 [Server Side Validation](https://getbootstrap.com/docs/4.1/components/forms/#server-side)
#
# TODO: https://jira.tiu11.org/browse/EITAGBYS-32
# HOWTO: http://stackoverflow.com/a/25857095

ActionView::Base.field_error_proc = proc { |html_tag, _instance|
  fragment = Nokogiri::HTML.fragment(html_tag)
  fragment.elements.add_class 'is-invalid'
  fragment.to_html.html_safe
}
