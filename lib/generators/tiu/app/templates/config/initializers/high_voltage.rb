HighVoltage.configure do |config|
  # Serve up static pages via top-level routes.
  # https://github.com/thoughtbot/high_voltage#top-level-routes
  config.route_drawer = HighVoltage::RouteDrawers::Root
end
