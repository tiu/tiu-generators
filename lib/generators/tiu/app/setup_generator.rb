# frozen_string_literal: true

require 'helpers/generator_helper'

module Tiu
  module App
    # Scaffolds ActiveJob
    class SetupGenerator < Rails::Generators::Base
      include ::Helpers::GeneratorHelper
      source_root File.expand_path('templates', __dir__)

      # Every method that is declared below will be automatically executed when the generator is run

      def copy_templates
        directory 'app'
        directory 'config'
        directory 'lib'
        directory 'spec'
        directory 'test'
        copy_file 'CHANGELOG.md'
        copy_file 'README.md'
      end

      def dependencies
        say "Installing JavaScript dependencies"
        run "yarn add bootstrap@^4.3"
        run "yarn add flatpickr@^4.6.6"
        run "yarn add jquery@^3.5.1"
        run "yarn add popper.js@^1.16.0"
      end
    end
  end
end
