# frozen_string_literal: true

require 'helpers/generator_helper'
require "rails/generators/active_record"

module Tiu
  module PgSearch
    # Scaffolds pg_search
    class InstallGenerator < Rails::Generators::Base
      include ::Helpers::GeneratorHelper
      include ::Rails::Generators::Migration
      source_root File.expand_path('templates', __dir__)

      def self.next_migration_number(dirname)
        ::ActiveRecord::Generators::Base.next_migration_number(dirname)
      end

      # Every method that is declared below will be automatically executed when the generator is run

      # Verify dependencies specific to this generator
      def dependencies
        return if gem_installed?('pg_search', '>= 2.3') && behavior == :invoke

        gem 'pg_search', '~> 2.3'
        insert_into_file 'Gemfile', %(# Use PostgreSQL's full text search),
                         after: /gem 'pg_search', '~> 2.3'$/
        run 'bundle install'
      end

      def migrate
        migration_template 'db/migrate/enable_fuzzystrmatch_extension.rb',
                           'db/migrate/enable_fuzzystrmatch_extension.rb'
        migration_template 'db/migrate/enable_stats_extension.rb',
                           'db/migrate/enable_stats_extension.rb'
        migration_template 'db/migrate/enable_unaccent_extension.rb',
                           'db/migrate/enable_unaccent_extension.rb'

        generate 'pg_search:migration:multisearch'

        # TODO: can we make this automatic?
        case behavior
        when :invoke
          say 'Run `db:migrate`'
        when :revoke
          warn 'Run `db:migrate:down VERSION=####`'
          warn 'Delete db/migrate/####_create_pg_search_documents.rb'
        end
      end

      def copy_files
        directory 'app'
        directory 'config'
      end

      # TODO: consider resourceful route for pg_search with path '/search', avoiding folder mismatches.
      def configure
        route %(get 'search', to: 'search#index')
      end
    end
  end
end
