# Additional options are good too. Requires the following Postgresql extensions:
# * [unaccent](https://www.postgresql.org/docs/current/static/unaccent.html)
# * :trigram via [pg_trgm](https://www.postgresql.org/docs/current/static/pgtrgm.html)
# * :dmetaphone via [fuzzystrmatch](https://www.postgresql.org/docs/current/static/fuzzystrmatch.html)
#
# (https://github.com/Casecommons/pg_search/wiki/Installing-PostgreSQL-Extensions)
PgSearch.multisearch_options = {
  using: {
    tsearch: {
      # Match partial words
      # * https://github.com/Casecommons/pg_search#prefix-postgresql-84-and-newer-only
      prefix: true,

      # Enable stemming so variants of words (e.g. "jumping" and "jumped") will match.
      # * https://github.com/Casecommons/pg_search#dictionary
      # * https://www.postgresql.org/docs/current/textsearch-dictionaries.html
      dictionary: 'english',

      # Exclude words that begin with '!'.
      # * https://github.com/Casecommons/pg_search#negation
      negation: true,

      # Set result highlighting options.
      # * https://github.com/Casecommons/pg_search#highlight
      # * https://www.postgresql.org/docs/current/textsearch-controls.html
      highlight: {
        StartSel: '<mark>',
        StopSel: '</mark>',
        MaxFragments: 3,
        FragmentDelimiter: '&hellip;'
      }
    }

    # Match with trigrams.
    # * https://github.com/Casecommons/pg_search#trigram-trigram-search
    # * https://www.postgresql.org/docs/current/pgtrgm.html
    # trigram: {}, # trigram does not use tsvectors

    # Double Metaphone matches soundalike words with different spellings (e.g. Jeff, Geoff).
    # * https://github.com/Casecommons/pg_search#dmetaphone-double-metaphone-soundalike-search
    # * http://www.postgresql.org/docs/current/static/fuzzystrmatch.html
    # * Enablin
    # dmetaphone: {}
  },

  # Ignore accent marks when searching (e.g. "piñata", "pinata")
  # * https://github.com/Casecommons/pg_search#ignoring-accent-marks
  # * https://www.postgresql.org/docs/current/static/unaccent.html
  ignoring: :accents
}
