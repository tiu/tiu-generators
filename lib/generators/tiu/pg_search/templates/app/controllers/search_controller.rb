# frozen_string_literal: true

class SearchController < ApplicationController
  skip_authorization_check

  def index
    @search_terms = search_params[:q] # TODO: wrap parameters instead of pulling them out?
    @results = PgSearch.multisearch(@search_terms).with_pg_search_highlight.includes(:searchable)
    @results = @results.accessible_by(current_ability).page(page_params)
  end

  private

    # Only allow a list of trusted parameters through.
    def search_params
      params.permit(:q, :utf8, :locale, :page)
    end

end
