# frozen_string_literal: true

require 'helpers/generator_helper'

module Tiu
  module Model
    # Generate Address model
    class PhoneGenerator < Rails::Generators::Base
      include ::Helpers::GeneratorHelper
      source_root File.expand_path('templates', __dir__)

      def phone
        generate 'model', 'Phone callable:belongs_to{polymorphic} number:string extension:string tag:string note:string sms:boolean carrier:string'
        copy_file 'app/helpers/phones_helper.rb'
        copy_file 'app/models/phone.rb'
        copy_file 'app/views/phones/_phone_fields.html.erb'
        copy_file 'app/views/phones/_phone.html.erb'
        copy_file 'spec/factories/phones.rb'
        copy_file 'spec/models/phone_spec.rb'
      end

      # Verify dependencies specific to this generator
      def check_dependencies
        gem 'phony_rails' # E164 phone number normalizing, validating, formatting
      end

      def register_types
        copy_file 'lib/types/type/phone_number.rb'
        append_to_file 'config/initializers/types.rb', <<~CONFIG
          ActiveRecord::Type.register(:phone_number, Type::PhoneNumber)
          ActiveModel::Type.register(:phone_number, Type::PhoneNumber)
        CONFIG
      end
    end
  end
end
