# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Address, type: :model do
  it "catches invalid zip codes" do
    address = FactoryBot.build(:address)
    invalid_zips = ['1234', '12345+', 'word']
    invalid_zips.each do |zip|
      address.zip = zip
      expect(address).not_to be_valid
    end
  end

  it "catches invalid states" do
    address = FactoryBot.build(:address)
    invalid_states = %w[AA 42 penna 456789]
    invalid_states.each do |state|
      address.state = state
      expect(address).not_to be_valid
    end
  end

  it "allows zip code extension" do
    address = FactoryBot.build(:address, zip: '17051-9434')
    expect(address).to be_valid
  end

  it "adds a dash to zip" do
    address = FactoryBot.build(:address, zip: '170519434')
    expect(address.zip).to match(/-/)
  end
end
