# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Link, type: :model do
  it "autodetects known social sites from a link" do
    links = [
      { type: 'amazon', uri: 'https://www.amazon.com/photos/all/gallery/oH2DpMiHQaq1QO3YPzrvCg' },
      { type: 'itunes', uri: 'https://itunes.apple.com/us/album/thunder-official-remix-single/1277886335' },
      { type: 'itunes', uri: 'https://itunes.apple.com/us/app/eita-mobile/id1369189145?ls=1&mt=8' },
      { type: 'itunes', uri: 'https://itunes.apple.com/us/app/macos-high-sierra/id1246284741?mt=12' },
      { type: 'itunes', uri: 'https://itunes.apple.com/us/artist/imagine-dragons/358714030' },
      { type: 'bitbucket', uri: 'https://bitbucket.org/tiu/eita-lol-course-tool/src' },
      { type: 'facebook-messenger', uri: 'https://m.me/tiu11edtech' },
      { type: 'facebook-messenger', uri: 'https://www.messenger.com/t/tiu11edtech' },
      { type: 'facebook', uri: 'https://fb.me/tiu11edtech' },
      { type: 'facebook', uri: 'https://www.facebook.com/tiu11edtech' },
      { type: 'github', uri: 'https://github.com/TIU11/Basic-CMIS' },
      { type: 'google-drive', uri: 'https://drive.google.com/drive/folders/1H8vKJxfyTdloZkTyr7SUoFIyxdcr0Rjg' },
      { type: 'google-drive', uri: 'https://docs.google.com/document/d/1qGe0jB4CKLd1UZt-papV2jfVyTv9W1I_oVqQ5Nsf7Zk' },
      { type: 'google-drive',
        uri: 'https://docs.google.com/presentation/d/1ERVTXZbYBMZf-9Zk3YsWw2oV14C5j0i1PsVgGywSsAI' },
      { type: 'google-play', uri: 'https://play.google.com/store/apps/details?id=org.eitapa.mobileapp' },
      { type: 'instagram', uri: 'https://www.instagram.com/p/Be4QXu5FL__/?hl=en&taken-by=spacex' },
      { type: 'linkedin', uri: 'https://www.linkedin.com/company/tuscarora-intermediate-unit-11/' },
      { type: 'pinterest', uri: 'https://www.pinterest.com/tiu11/' },
      { type: 'slack', uri: 'https://tiu11.slack.com/stats' },
      { type: 'spotify', uri: 'https://open.spotify.com/album/1RlpachGbLBOZ2L1JK3N49' },
      { type: 'twitter', uri: 'https://twitter.com/TIU11EdTech' },
      { type: 'vimeo', uri: 'https://vimeo.com/spacex' },
      { type: 'youtube', uri: 'https://www.youtube.com/channel/UCPWarBSsuRhm9rKgVUArc2g' }
    ]

    links.each do |attrs|
      link = Link.new attrs[:uri]
      expect(link.type).to eq(attrs[:type])
    end
  end

  it "throws Addressable::URI::InvalidURIError" do
    exceptional_links = [
      'http://', '://'
    ]

    exceptional_links.each do |link|
      expect { Link.new link }.to raise_error Addressable::URI::InvalidURIError
    end
  end
end
