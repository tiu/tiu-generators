# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Type::Link, type: :model do

  # Define a test model. Subclass of self to namespace within this test.
  class self::ModelForType
    include ActiveModel::Model
    include ActiveModel::Attributes
    attribute :url, :link
  end

  it "allows valid uri's" do
    valid_uris = ['http://www.tiu11.org']
    valid_uris.each do |uri|
      model = self.class::ModelForType.new(url: uri)
      expect(model.url).to be_a(Addressable::URI)
      expect(model.url).to be_a(::Link)
    end
  end

end
