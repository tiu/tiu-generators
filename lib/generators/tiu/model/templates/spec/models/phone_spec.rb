# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Phone, type: :model do

  it "normalizes valid phone numbers" do
    phone = FactoryBot.build(:phone)
    valid_numbers = [
      '555 123.1234', '(555) 123-1234', '5551231234',
      '1(555)123-1234', '+1 555 123 1234',
      'my digits are 555.1231234, wow!'
    ]
    valid_numbers.each do |number|
      phone.number = number
      expect(phone).to be_valid
      expect(phone.formatted_number).to eq('(555) 123-1234')
      expect(phone.normalized_number).to eq('+15551231234')
      expect(phone.number).to eq('+15551231234')
    end
  end

  # TODO: should these be valid? ['1', 'not a number']
  it "catches invalid phone numbers" do
    phone = FactoryBot.build(:phone)
    invalid_numbers = ['555-1234', '2 is not enough']
    invalid_numbers.each do |number|
      phone.number = number
      expect(phone).not_to be_valid
      expect(phone.errors[:number]).to contain_exactly "is an invalid number"
    end
  end

  it "retrieves phones even with non-normalized query values" do
    de_normalized_number = '(555) 123-1234'
    FactoryBot.create(:phone, number: de_normalized_number)

    phone = Phone.find_by! number: de_normalized_number
    expect(phone.number).not_to eq(de_normalized_number)
  end

  it "provides SMS and MMS email for known carriers" do
    # TODO: Phone::Carriers.each do |carrier|
    ['AT&T', 'Verizon'].each do |carrier|
      phone = FactoryBot.build(:phone, carrier: carrier, sms: true, extension: nil)
      expect(phone.sms_email).not_to be_empty
      expect(phone.mms_email).not_to be_empty
    end
  end

  it "accepts extensions with space" do
    phone = Phone.new(number: '5551231234', extension: '30 ')
    expect(phone).to be_valid
  end

  # TODO: change this to be valid after fix for https://github.com/rails/rails/issues/33431
  it "accepts extensions with space on update" do
    phone = Phone.new(number: '5551231234', extension: '30 ')
    expect(phone).to be_valid

    phone.save
    phone.extension = '30 '
    expect(phone).not_to be_valid
  end
end
