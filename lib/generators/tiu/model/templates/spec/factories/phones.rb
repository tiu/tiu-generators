# frozen_string_literal: true

FactoryBot.define do
  factory :phone do
    callable { nil }
    number { Faker::PhoneNumber.cell_phone }
    extension { rand(3..1000) }
    tag { Phone::TAGS.sample }
    note { "Please don't call after 7pm" }
  end
end
