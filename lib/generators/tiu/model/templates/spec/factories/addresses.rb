# frozen_string_literal: true

FactoryBot.define do
  factory :address do
    line1 { '2527 US Highway 522 South' }
    city { 'McVeytown' }
    state { 'PA' }
    zip { '17051' }
  end
end
