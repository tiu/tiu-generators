# frozen_string_literal: true

class Address < ApplicationRecord
  has_paper_trail

  # include PgSearch
  # multisearchable against: [:model_name, :line1, :line2, :city, :state, :zip]

  alias_attribute :street, :line1

  attribute :line1, :string, squish: true
  attribute :line2, :string, squish: true
  attribute :city, :string, squish: true
  attribute :state, :state_code
  attribute :zip, :zip

  #
  # Associations
  #

  belongs_to :addressable, polymorphic: true, optional: true

  #
  # Scopes
  #

  scope :sorted, -> { order(:zip) }
  scope :orphan, -> { where(addressable_id: nil) }

  #
  # Callbacks
  #

  #
  # Validations
  #

  validates :line1, :city, :state, :zip, presence: true, unless: :blank?
  validates :state, length: { is: 2 }, inclusion: { in: proc { Type::StateCode::US_STATE_CODES } }, allow_blank: true
  validates :zip, format: { with: /\A\d{5}(-\d{4})?\z/ }, allow_blank: true

  #
  # Class Methods
  #

  class << self
    def reject?(attributes)
      exists = attributes['id'].present?
      empty = attributes.except(:id).values.all?(&:blank?)
      attributes[:_destroy] = 1 if exists && empty # destroy empty address

      # reject empty attributes
      !exists && empty
    end
  end

  #
  # Methods
  #

  def blank?
    attributes.except(:id).values.all?(&:blank?)
  end

  def complete?
    [line1, city, state, zip].all?(:present?)
  end

  def to_s
    "#{[line1, line2, city, state].compact_blank.join(', ')} #{zip}"
  end
end
