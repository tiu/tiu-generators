# frozen_string_literal: true

# Uses PhonyRails to normalize and format phone numbers. This is more capable
# than ActionView::Helpers::NumberHelper.
#
# == Usage
#     class User
#       has_one :phone
#       validates_associated :phone
#     end
class Phone < ApplicationRecord
  TAGS = %w[Home Work Mobile Fax Business Other].freeze
  COUNTRY_CODE = 'US'

  has_paper_trail

  # include PgSearch
  # multisearchable against: [:model_name, :tag, :number, :extension]

  alias_attribute :ext, :extension
  alias_attribute :normalized_number, :number
  alias_attribute :formatted_number, :number
  attribute :number, :phone_number
  attribute :extension, :string, squish: true

  #
  # Associations
  #

  belongs_to :callable, polymorphic: true, optional: true

  #
  # Scopes
  #

  scope :sorted, -> { order('tag DESC', :number) }
  scope :orphan, -> { where(callable_id: nil) }

  #
  # Validations
  #

  validates :number, phony_plausible: true
  validates :number, uniqueness: { scope: [:callable, :extension, :tag] }, presence: true
  validates :tag, inclusion: { in: Phone::TAGS }, allow_blank: true
  validates :extension, numericality: { only_integer: true }, allow_blank: true
  validates :carrier, presence: true, if: :sms?

  #
  # Class Methods
  #

  class << self
    def reject?(attributes)
      exists = attributes['id'].present?
      empty = attributes.except(:id, :tag, :_destroy).values.all?(&:blank?)
      attributes[:_destroy] = 1 if exists && empty # destroy empty address

      # reject empty attributes
      !exists && empty
    end

    def tags
      TAGS
    end
  end

  #
  # Methods
  #

  # if we have a value return that, else return the formatted number from the database column :number
  def formatted_number
    normalized_number&.phony_formatted
  end

  # Formatted for href, as when displaying a callable phone number
  def tel
    ["tel:#{normalized_number}", extension].compact_blank.join('p')
  end

  # References:
  # * https://en.wikipedia.org/wiki/List_of_United_States_wireless_communications_service_providers
  # * https://en.wikipedia.org/wiki/List_of_United_States_mobile_virtual_network_operators
  CARRIER_TXT_EMAIL = {
    'AT&T' =>                   { sms: 'txt.att.net', mms: 'mms.att.net' },
    'Boost Mobile' =>           { sms: 'sms.myboostmobile.com', mms: 'myboostmobile.com' },
    'C-Spire' =>                { sms: 'cspire1.com' },
    'Consumer Cellular' =>      { sms: 'txt.att.net', mms: 'mms.att.net' }, # an AT&T MVNO
    'Cricket' =>                { sms: 'sms.cricketwireless.net', mms: 'mms.cricketwireless.net' },
    'Google Fi (Project Fi)' => { sms: 'msg.fi.google.com', mms: 'msg.fi.google.com' },
    'Metro PCS' =>              { sms: 'mymetropcs.com', mms: 'mymetropcs.com' },
    'Page Plus' =>              { sms: 'vtext.com', mms: 'vzwpix.com' }, # a Verizon MVNO
    'Republic Wireless' =>      { sms: 'text.republicwireless.com' },
    'Sprint' =>                 { sms: 'messaging.sprintpcs.com', mms: 'pm.sprint.com' }, # sms: messaging.sprintpcs.com
    'T-Mobile' =>               { sms: 'tmomail.net', mms: 'tmomail.net' },
    'Ting' =>                   { sms: 'message.ting.com' }, # a Sprint MVNO
    # 'Tracfone' =>               { mms: 'mmst5.tracfone.com' },
    'U.S. Cellular' =>          { sms: 'email.uscc.net', mms: 'mms.uscc.net' },
    'Verizon' =>                { sms: 'vtext.com', mms: 'vzwpix.com' },
    'Virgin Mobile' =>          { sms: 'vmobl.com', mms: 'vmpix.com' }
  }.freeze

  def sms_email
    return unless carrier.present? && extension.blank?

    domain = CARRIER_TXT_EMAIL[carrier] && CARRIER_TXT_EMAIL[carrier][:sms]
    [number.last(10), domain].join '@'
  end

  def mms_email
    return unless carrier.present? && extension.blank?

    domain = CARRIER_TXT_EMAIL[carrier] && CARRIER_TXT_EMAIL[carrier][:mms]
    [number.last(10), domain].join '@'
  end

  # The two-letter country code as per ISO 3166-1 alpha-2
  def country_code
    COUNTRY_CODE
  end

  def to_s
    [formatted_number, extension].compact_blank.join(' x ')
  end
end
