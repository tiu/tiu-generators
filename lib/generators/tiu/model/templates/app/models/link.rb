# frozen_string_literal: true

# Extends Addressable::URI with ability to initilize from a String
#
# +type+ Name of known sites
class Link < Addressable::URI
  include ActiveModel::Model
  # Sites:
  # * https://github.com/huacnlee/social-share-button
  # * https://richonrails.com/articles/adding-social-sharing-buttons-to-your-rails-app

  attr_reader :type

  TYPE_BY_DOMAIN = {
    'amazon.com' => 'amazon',
    'adobe.com' => 'adobe',
    'apple.com' => 'apple',
    'behance.net' => 'behance',
    'facebook.com' => 'facebook',
    'fb.me' => 'facebook',
    'instagram.com' => 'instagram',
    'linkedin.com' => 'linkedin',
    'm.me' => 'facebook-messenger',
    'messenger.com' => 'facebook-messenger',
    'pinterest.com' => 'pinterest',
    'reddit.com' => 'reddit',
    'skype.com' => 'skype',
    'slack.com' => 'slack',
    'spotify.com' => 'spotify',
    'twitter.com' => 'twitter',
    'vimeo.com' => 'vimeo',
    'whatsapp.com' => 'whatsapp',
    'youtube.com' => 'youtube',

    'bitbucket.org' => 'bitbucket',
    'dropbox.com' => 'dropbox',
    'github.com' => 'github',
    'gitlab.com' => 'gitlab',
    'office.com' => 'microsoft'
  }.freeze

  TYPE_BY_HOST = {
    'docs.google.com' => 'google-drive',
    'drive.google.com' => 'google-drive',
    'itunes.apple.com' => 'itunes',
    'play.google.com' => 'google-play'
  }.freeze

  def initialize(value)
    if value.is_a? ::String # TODO: ::String, ::URI, Addressable::URI, ::Hash
      super Addressable::URI.heuristic_parse(value, scheme: 'https').to_hash
    else
      super
    end
    update_type
  end

  private

    def update_type
      @type = TYPE_BY_HOST[host] || TYPE_BY_DOMAIN[domain]
    end
end
