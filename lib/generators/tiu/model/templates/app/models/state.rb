# frozen_string_literal: true

class State < ApplicationRecord
  include FriendlyId
  friendly_id :postal_code

  #
  # Scopes
  #

  scope :sorted, -> { order(:name) }

  #
  # Validations
  #

  validates :name, :fips_code, :postal_code, presence: true

  #
  # Methods
  #

  def to_s
    name
  end
end
