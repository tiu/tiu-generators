# frozen_string_literal: true

module LinkHelper

  SITES = {
    'amazon'       => { domain: 'amazon.com' },
    'itunes' => { icon: 'itunes', host: 'itunes.apple.com' },
    'apple'        => { domain: 'apple.com' },
    'behance'      => { domain: 'behance.com' },
    'bitbucket'    => { domain: 'bitbucket.org' },
    'dropbox'      => { domain: 'dropbox.com' },
    'facebook-messenger' => { domain: ['messenger.com', 'm.me'] },
    'facebook'     => { domain: ['facebook.com', 'fb.me'] },
    'github'       => { domain: 'github.com' },
    'gitlab'       => { domain: 'gitlab.com' },
    'google-drive' => { host: ['drive.google.com', 'docs.google.com'] },
    'google-play'  => { host: 'play.google.com' },
    'instagram'    => { domain: 'instagram.com' },
    'linkedin'     => { domain: 'linkedin.com' },
    'microsoft-office' => { icon: 'microsoft', domain: 'office.com' },
    'pinterest'    => { domain: 'pinterest.com' },
    'reddit'       => { domain: 'reddit.com' },
    'skype'        => { domain: 'skype.com' },
    'slack'        => { domain: 'slack.com' },
    'stack-overflow' => { domain: 'stackoverflow.com' },
    'spotify'      => { domain: 'spotify.com' },
    'twitter'      => { domain: 'twitter.com' },
    'vimeo'        => { domain: 'vimeo.com' },
    'whatsapp'     => { domain: 'whatsapp.com' },
    'youtube'      => { domain: 'youtube.com' }
  }.freeze

  # Transpose SITES hash to be keyed by a value (e.g :domain or :host)
  # Flattens value arrays, so each value becomes a key.
  def self.transpose_sites_by(name)
    SITES.flat_map do |site, value_hash|
      Array(value_hash[name]).map do |value|
        [value, site]
      end
    end.to_h
  end

  # TODO: +link_to+ doesn't accept an +Addressable::URI+, but I think it should. Consider an override or a pull request.
  # TODO: don't make it a link if it is not a link
  def online_link(uri, options = {})
    options = {
      target: '_blank', rel: 'noopener', title: uri
    }.deep_merge(options)

    link_to link_icon(uri), uri.to_s, options
  end

  def email_link(email, options = {})
    options = {
      target: '_blank', rel: 'noopener'
    }.deep_merge(options)

    mail_to email, icon('fas', 'envelope'), options
  end

  # Define HOSTS and DOMAINS when they are first accessed. This can't be done
  # directly since they're defined using a method on self.
  def self.const_missing(sym)
    case sym
    when :HOSTS
      const_set sym, transpose_sites_by(:host).freeze
    when :DOMAINS
      const_set sym, transpose_sites_by(:domain).freeze
    else
      super
    end
  end

  private

    def link_icon(uri)
      type = link_type(uri)
      if SITES[type]
        icon_name = SITES[type][:icon] || type
        icon('fab', icon_name)
      else
        icon('fas', 'globe')
      end
    end

    def link_type(uri)
      HOSTS[uri&.host] || DOMAINS[uri&.domain]
    end

end
