# frozen_string_literal: true

module PhonesHelper
  ICONS = {
    'Fax' => 'fax',
    'Mobile' => 'mobile-alt'
  }.freeze
  DEFAULT_ICON = 'phone'

  # Ex. `phone_icon(phone, class: 'fa-fw')`
  def phone_icon(phone, **options)
    icon_name = ICONS[phone.tag] || DEFAULT_ICON
    icon('fas', icon_name, title: phone.tag, 'data-bs-toggle': 'tooltip', **options)
  end
end
