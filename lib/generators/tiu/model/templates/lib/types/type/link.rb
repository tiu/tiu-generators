# frozen_string_literal: true

module Type
  class Link < ActiveModel::Type::String

    def serialize(value)
      value.to_s
    end

    private

      def cast_value(value)
        ::Link.new(value)
      rescue Addressable::URI::InvalidURIError
        value
      end
  end
end
