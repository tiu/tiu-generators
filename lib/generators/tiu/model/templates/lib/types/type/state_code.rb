# frozen_string_literal: true

module Type
  class StateCode < ActiveRecord::Type::String

    # Define dynamic constants when they're first accessed.
    def self.const_missing(sym)
      case sym
      when :US_STATES_BY_CODE
        states_by_code = ::State.pluck(:postal_code, :name).to_h.freeze
        const_set sym, states_by_code unless states_by_code.blank?
      when :US_STATES_BY_NAME
        return if US_STATES_BY_CODE.blank?
        const_set sym, US_STATES_BY_CODE.invert.freeze
      when :US_STATE_CODES
        return if US_STATES_BY_CODE.blank?
        const_set sym, US_STATES_BY_CODE.keys.to_set.freeze
      end
    end

    private

      # Examples:
      # * :pa => 'PA'
      # * 'Pa' => 'PA'
      # * :pennsylvania => 'PA'
      # TODO: :titleize incorrectly formats "District of Columbia"
      def cast_value(value)
        raise "State must be populated first. Try `UpdateStates.call`" unless US_STATE_CODES.present?

        val = value.to_s&.squish&.upcase
        return val if US_STATE_CODES.include? val

        US_STATES_BY_NAME[val&.titleize] || value
      end
  end
end
