# frozen_string_literal: true

require 'helpers/generator_helper'

module Tiu
  module Model
    # Generate Address model
    class AddressGenerator < Rails::Generators::Base
      include ::Helpers::GeneratorHelper
      source_root File.expand_path('templates', __dir__)

      def address
        generate 'migration', 'CreateAddresses addressable:belongs_to{polymorphic} line1:string line2:string city:string state:string{2} zip:string{10}'
        copy_file 'app/models/address.rb'
        copy_file 'app/views/addresses/_address_fields.html.erb'
        copy_file 'app/views/addresses/_address.html.erb'
        copy_file 'spec/factories/addresses.rb'
        copy_file 'spec/models/address_spec.rb'
      end

      def state
        generate 'migration', 'CreateStates name:string fips_code:string postal_code:string gnis_id:integer'
        copy_file 'app/models/state.rb'
        copy_file 'spec/fixtures/states.yml'
        copy_file 'test/fixtures/states.yml'

        # See https://brandonhilkert.com/blog/using-rails-fixtures-to-seed-a-database/
        append_to_file 'db/seeds.rb', <<~CONFIG
          # States
          # UpdateStates.call
          # TODO: maybe just load a fixture from db/seeds/states.yml
          # ENV['FIXTURES'] = 'states'
          # Rails::Command.invoke 'db:fixtures:load'
        CONFIG
      end

      def register_types
        copy_file 'lib/types/type/state_code.rb'
        copy_file 'lib/types/type/zip.rb'
        append_to_file 'config/initializers/types.rb', <<~CONFIG
          ActiveRecord::Type.register(:state_code, Type::StateCode)
          ActiveRecord::Type.register(:zip, Type::Zip)
          ActiveModel::Type.register(:state_code, Type::StateCode)
          ActiveModel::Type.register(:zip, Type::Zip)
        CONFIG
      end
    end
  end
end
