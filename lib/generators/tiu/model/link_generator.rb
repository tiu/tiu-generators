# frozen_string_literal: true

require 'helpers/generator_helper'

module Tiu
  module Model
    # Generate Address model
    class LinkGenerator < Rails::Generators::Base
      include ::Helpers::GeneratorHelper
      source_root File.expand_path('templates', __dir__)

      # Verify dependencies specific to this generator
      def dependencies
        gem 'addressable', '~> 2.6'
        insert_into_file 'Gemfile', '     # more conformant replacement for (obsolete) Ruby URI',
                         after: /'addressable', '~> 2.6'$/
      end

      def copy_files
        copy_file 'app/helpers/link_helper.rb'
        copy_file 'app/models/link.rb'
        copy_file 'spec/models/link_spec.rb'
        copy_file 'spec/models/type/link_spec.rb'
      end

      def register_types
        copy_file 'lib/types/type/link.rb'
        append_to_file 'config/initializers/types.rb', <<~CONFIG
          ActiveRecord::Type.register(:link, Type::Link)
          ActiveModel::Type.register(:link, Type::Link)
        CONFIG
      end
    end
  end
end
