require 'rails_helper'

# Reference:
# - [Job spec](https://relishapp.com/rspec/rspec-rails/docs/job-specs/job-spec)
# - [Block implementation](https://relishapp.com/rspec/rspec-mocks/docs/configuring-responses/block-implementation)
RSpec.describe EmailWelcomeUserJob, type: :job do

  context 'with the :test adapter and test helpers' do
    include ActiveJob::TestHelper

    it 'discards jobs when the record cannot be deserialized' do
      allow(ActiveJob::Base.logger).to receive(:error)
      non_existant_record = User.new id: (User.maximum(:id).to_i + 1)

      perform_enqueued_jobs do
        EmailWelcomeUserJob.perform_later non_existant_record
      end

      expect(EmailWelcomeUserJob).to have_been_performed.once
      expect(ActiveJob::Base.logger).to have_received(:error) { |_msg, &block|
        expect(block.call).to eq "Discarded EmailWelcomeUserJob due to a ActiveJob::DeserializationError." if block
      }
    end
  end

  context 'with the :delayed_job adapter and no test helpers', :delayed_job do
    it 'sends the Welcome email when a User is created' do
      expect_any_instance_of(EmailWelcomeUserJob).to receive(:perform).once.and_call_original

      FactoryBot.create :user, send_welcome_email: true

      expect(ActionMailer::Base.deliveries.size).to eq 1
      mail = ActionMailer::Base.deliveries.first
      expect(mail.subject).to eq "Welcome to #{I18n.t('app.title')}"
    end

    it 'is discarded when the record cannot be deserialized' do
      non_existant_record = User.new id: (User.maximum(:id).to_i + 1)
      allow(ActiveJob::Base).to receive(:execute).once.and_call_original
      expect_any_instance_of(EmailWelcomeUserJob).not_to receive(:perform)
      allow(ActiveJob::Base.logger).to receive(:error).and_call_original

      EmailWelcomeUserJob.perform_later non_existant_record

      expect(ActiveJob::Base).to have_received(:execute).with(hash_including('job_class' => 'EmailWelcomeUserJob'))
      expect(ActiveJob::Base.logger).to have_received(:error) { |_msg, &block|
        expect(block.call).to eq "Discarded EmailWelcomeUserJob due to a ActiveJob::DeserializationError." if block
      }
    end

    it 'is retried on errors' do
      Delayed::Worker.delay_jobs = true
      error = Net::SMTPAuthenticationError
      max_attempts = Delayed::Worker.max_attempts
      allow(ActiveJob::Base).to receive(:execute).and_call_original
      allow_any_instance_of(EmailWelcomeUserJob).to receive(:perform).and_raise(error)
      allow(ActiveJob::Base.logger).to receive(:error).and_call_original

      FactoryBot.create :user, send_welcome_email: true

      worker = Delayed::Worker.new
      delayed_job = Delayed::Job.first
      max_attempts.times do |attempts|
        expect(Delayed::Job.count).to eq 1
        expect(Delayed::Job.pending.count).to eq 1 if attempts.zero?
        expect(Delayed::Job.failed.count).to eq 1 if attempts.positive?
        expect(Delayed::Job.troubled.count).to eq 1 if attempts.positive?
        expect(delayed_job.attempts).to eq attempts
        result = worker.run(delayed_job)
        expect(result).to be false # job fails
      end

      expect(Delayed::Job.count).to eq 0 # Failed job was dropped
      expect(ActiveJob::Base).to have_received(:execute).exactly(max_attempts).times
      expect(ActionMailer::Base.deliveries.size).to eq max_attempts
    end
  end
end
