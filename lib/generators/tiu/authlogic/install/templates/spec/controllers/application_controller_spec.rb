require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do

  describe "anynomous controller" do
    controller do
      def index
        redirect_to params[:redirect_uri]
      end

      def create
        redirect_back_or_default_to root_url # see ApplicationController method
      end
    end

    context "with external url" do
      let(:redirect_uri) { 'https://example.com' }

      # new in Rails 7, Open Redirect protection on by default
      it "raises an error when using the default redirect_to" do
        expect do
          get :index, params: { redirect_uri: redirect_uri }
        end.to raise_error(ActionController::Redirecting::UnsafeRedirectError)
      end

      it "does not raise an error when url_from is used with redirect_uri" do
        expect do
          post :create, params: { redirect_uri: redirect_uri }
        end.not_to raise_error # => ActionController::Redirecting::UnsafeRedirectError
      end
    end
  end
end
