require 'rails_helper'

RSpec.describe UserSessionsController, type: :controller do

  describe "User logging in" do
    context "with external url" do
      let(:test_password) { 'Test12345678' }
      let(:user) { FactoryBot.create :user, password: test_password, password_confirmation: test_password }
      let(:redirect_uri) { 'https://google.com' }

      it 'redirect to root_url when session return_to set' do

        post :create, params:
              {
                user_session:
                  { login: user.email, password: test_password }
              },
             session: { return_to: redirect_uri }

        expect(response).to redirect_to(root_url)
        # if this test fails it may raise UnsafeRedirectError
      end

      it "redirect to root_url when params redirect_uri set" do

        post :create,
             params:
              {
                user_session:
                  { login: user.email, password: test_password },
                redirect_uri: redirect_uri
              }
        expect(response).to redirect_to(root_url)
      end
    end
  end
end
