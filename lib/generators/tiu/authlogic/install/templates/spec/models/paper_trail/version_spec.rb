require 'rails_helper'

RSpec.describe PaperTrail::Version, type: :model do
  describe "#enhanced_changeset" do
    it "contains attribute changes" do
      user = FactoryBot.create :user
      user.update first_name: "Jerry"

      version = PaperTrail::Version.sorted.find_by(item: user)

      expect(version.event).to eq 'update'
      expected_change = user.previous_changes.except(*user.paper_trail_options.values_at(:ignore, :skip).flatten)
      expect(version.enhanced_changeset).to match(expected_change)
    end

    it "mimics attributes being cleared on 'destroy'" do
      user = FactoryBot.create :user
      user.destroy

      version = PaperTrail::Version.sorted.find_by(item: user)

      expect(version.event).to eq 'destroy'
      expect(version.enhanced_changeset.keys).to match(version.object.compact_blank.keys)
    end

    it "is empty on 'touch'" do
      user = FactoryBot.create :user
      user.paper_trail_options[:ignore] = [] # Temporarily stop ignoring
      user.touch

      version = PaperTrail::Version.sorted.find_by(item: user)

      expect(version.event).to eq 'update'
      expect(version.enhanced_changeset).to eq({})
    end
  end
end
