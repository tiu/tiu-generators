require 'rails_helper'

include ActiveSupport::Testing::TimeHelpers

RSpec.describe User, type: :model do

  it "welcomes new users" do
    ActiveJob::Base.queue_adapter = :test

    FactoryBot.create :user, send_welcome_email: true
    expect(EmailWelcomeUserJob).to have_been_enqueued
  end

  it "sends password reset request" do
    ActiveJob::Base.queue_adapter = :test

    user = FactoryBot.create(:user)
    EmailPasswordResetJob.perform_later(user)
    expect(EmailPasswordResetJob).to have_been_enqueued
  end

  it "has a name and email" do
    user = FactoryBot.build(:user)
    expect(user.name).to eq('George P Burdell')
    expect(user.first_initial).to eq('G')
    expect(user.full_email).to match(/George P Burdell <g.p.burdell-\d+@example.com>/)
  end

  it "gets a username and password before validation" do
    user = FactoryBot.build(:user, password: nil, password_confirmation: nil)
    expect(user.password).to be_nil
    expect(user.username).to be_nil
    user.validate
    expect(user.password).not_to be_nil
    expect(user.username).to eq('gburdell')
  end

  it "accepts valid emails" do
    user = FactoryBot.build(:user)
    valid_emails = %w[mail@example.com mail+test-a@example.co.uk mail@大众汽车.example.cn ßèâñÿ@example.com]
    valid_emails.each do |email|
      user.email = email
      expect(user).to be_valid
    end
  end

  it "rejects invalid emails" do
    user = FactoryBot.build(:user)
    invalid_emails = %w[mail @example.com example.com mail@example mail@example.c mail@example,com]
    invalid_emails.each do |email|
      user.email = email
      expect(user).not_to be_valid
      expect(user.errors[:email]).to contain_exactly I18n.t('errors.attributes.email.invalid')
    end
  end

  describe '#perishable_token_stale?' do
    it 'returns true until :perishable_token_valid_for passes since last update' do
      user = FactoryBot.build(:user)
      expect(user.perishable_token_stale?).to be true
      user.save
      expect(user.perishable_token_stale?).to be false
      travel User.perishable_token_valid_for + 1
      expect(user.perishable_token_stale?).to be true
    end
  end

  describe '#disable_perishable_token_maintenance?' do
    it 'returns true until :perishable_token_valid_for passes since last update' do
      user = FactoryBot.build(:user)
      expect(user.disable_perishable_token_maintenance?).to be false
      user.save
      expect(user.disable_perishable_token_maintenance?).to be true
      travel User.perishable_token_valid_for + 1
      expect(user.disable_perishable_token_maintenance?).to be false
    end
  end

end
