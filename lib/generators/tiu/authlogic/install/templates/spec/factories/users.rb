# frozen_string_literal: true

FactoryBot.define do

  # normal user
  factory :user do
    password = SecureRandom.base64(25)

    sequence(:email) { |n| "g.p.burdell-#{n}@example.com" }
    first_name { 'George' }
    middle_name { 'P' }
    last_name { 'Burdell' }
    password { password }
    password_confirmation { password }

    # Administrator user
    factory :admin do
      roles { [:administrator] }
    end

    # System Administrator user
    factory :sysadmin do
      roles { [:system_administrator] }
    end
  end
end
