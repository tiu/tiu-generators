# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers
class UserMailerPreview < ActionMailer::Preview
  def welcome
    user = User.first
    UserMailer.welcome(user)
  end

  def reset_password_instructions
    user = User.first
    UserMailer.reset_password_instructions(user)
  end
end
