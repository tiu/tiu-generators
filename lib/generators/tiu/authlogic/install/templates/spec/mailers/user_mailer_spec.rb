# frozen_string_literal: true

require 'rails_helper'

# Reference:
# - https://relishapp.com/rspec/rspec-rails/docs/mailer-specs
RSpec.describe UserMailer, type: :mailer do
  describe "Welcome" do
    let(:user) { FactoryBot.create(:user) }
    let(:mail) { UserMailer.welcome(user).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eql("Welcome to #{I18n.t('app.title')}")
    end

    it 'renders the receiver email' do
      expect(mail.to).to eql([user.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to all end_with('@tiu11.org')
    end

    it 'renders the reply to email and name' do
      expect(mail[:reply_to].display_names).to eql(["#{I18n.t('app.title')} TEST"])
      expect(mail.reply_to).to eql([I18n.t('app.support_email')])
    end

    it 'assigns @user' do
      expect(mail.body.encoded).to match(user.name)
    end

    it 'has analytics tags on all links' do
      # NOTE: this link appears in the signature
      expect(mail.text_part.body.decoded).to include("http://example.org/?utm_medium=email&utm_source=welcome")
    end
  end

  describe "Password Reset" do
    let(:user) { FactoryBot.create(:user) }
    let(:mail) { UserMailer.reset_password_instructions(user).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eql("#{I18n.t('app.title')} Password Update Link")
    end

    it 'assigns @user' do
      expect(mail.body.encoded).to match(user.username)
    end
  end
end
