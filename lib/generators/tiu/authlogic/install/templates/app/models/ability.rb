# frozen_string_literal: true

# Defines all user permissions. Initialized in +ApplicationController.current_ability+.
#
# BUG:
# Abilities based on associations can't handle `nil` associations.
# @see: https://github.com/ryanb/cancan/issues/213, https://github.com/rails/rails/issues/939
#
# See the wiki for details:
# https://github.com/CanCanCommunity/cancancan/wiki/defining-abilities
class Ability
  include CanCan::Ability

  def initialize(user, session = nil)
    alias_action :update, :destroy, to: :modify # https://github.com/CanCanCommunity/cancancan/wiki/Action-Aliases

    @user = user
    @session = session
    @user ? user_rules : public_rules
  end

  # Rules for logged-in users
  def user_rules
    public_rules # inherits public rules

    # Update my account
    can [:read, :update], User, id: @user.id

    # Apply rules for each role
    administrator_rules if @user.is? :administrator
    system_administrator_rules if @user.is? :system_administrator
    # reviewer_rules if @user.is? :reviewer
  end

  # Rules for public, un-authenticated users
  def public_rules
    # can :read, Example
  end

  #
  # Role-based rules
  #

  # Defines abilities for the "Administrator" role.
  def administrator_rules
    can :manage, :all
    cannot :revert, PaperTrail::Version

    cannot :manage, Delayed::Job
    can :read, Delayed::Job
  end

  # Defines abilities for the "System Administrator" role.
  def system_administrator_rules
    administrator_rules # inherits administrator rules
    can :revert, PaperTrail::Version
    can :manage, Delayed::Job
  end

  # def reviewer_rules
  #   can :read, :all
  # end
end
