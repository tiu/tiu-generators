# frozen_string_literal: true

class UserSession < Authlogic::Session::Base
  # See https://www.rubydoc.info/github/binarylogic/authlogic/Authlogic/Session/Password/Config
  record_selection_method :find_by_username_or_email
  login_field :login

  logout_on_timeout true
  generalize_credentials_error_messages true

  delegate :helpers, to: 'ActionController::Base'
  delegate :url_helpers, to: 'Rails.application.routes'

  #
  # Callbacks
  #

  # Always reset perishable_token after successful login
  after_create -> { record.reset_perishable_token }

  #
  # Validations
  #

  validate :first_time_user
  validate :forgetful_user

  private

    def first_time_user
      return unless errors.any? && attempted_record&.login_count&.zero?

      new_password_url = url_helpers.new_password_url(login: attempted_record.to_param)
      message = errors.generate_message(:base, :first_time_user, url: new_password_url)

      errors.add :base, helpers.sanitize(message)
    end

    def forgetful_user
      return unless errors.any? && attempted_record&.login_count&.positive? && attempted_record.failed_login_count > 2

      new_password_url = url_helpers.new_password_url(login: attempted_record.to_param)
      message = errors.generate_message(:base, :forgetful_user, url: new_password_url)

      errors.add :base, helpers.sanitize(message)
    end
end
