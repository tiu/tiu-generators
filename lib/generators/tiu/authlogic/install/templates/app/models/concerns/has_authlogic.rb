# Our standard extensions to Authlogic
module HasAuthlogic
  REPEATING_PASSWORD_REGEX = /(\w)\1{2}/.freeze # 3 or more of the same character

  def self.included(base)
    base.acts_as_authentic do |config|
      config.perishable_token_valid_for 1.day # for password change email
      config.logged_in_timeout 1.hour
      config.crypto_provider = ::Authlogic::CryptoProviders::SCrypt
    end

    base.include PerishableTokenMaintenance

    base.scope :recently_logged_in, -> {
      where('last_request_at > ?', logged_in_timeout.seconds.ago.beginning_of_day).where.not(current_login_at: nil)
    }
  end

  module PerishableTokenMaintenance
    # We want to maintain/reset :perishable_token when:
    # 1. password is being updated
    # 2. the current token/record has aged out
    #
    # So, we'll reset with any update coming over 1.day since the last update,
    # and anytime we change the password.
    def disable_perishable_token_maintenance?
      !crypted_password_changed? && !perishable_token_stale?
    end

    # Enough time has passed since the last update that the perishable token is no longer valid.
    def perishable_token_stale?
      updated_at.nil? || updated_at <= self.class.perishable_token_valid_for.to_i.seconds.ago
    end
  end

  # Assign user a default password
  def ensure_new_account_has_password
    return unless password.nil? && new_record?

    Rails.logger.debug { "Generating random password for #{email}." }
    random_password = nil

    5.times do
      random_password = SecureRandom.base64(25)
      break unless random_password.match(REPEATING_PASSWORD_REGEX)
    end

    self.password = random_password
    self.password_confirmation = random_password
  end

  # Validate password contains 3 of 4 character classes
  # https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/Authentication_Cheat_Sheet.md
  #
  # TODO: We shouldn't require certain character classes or expire passwords:
  # - https://pages.nist.gov/800-63-FAQ/#q-b05
  # - https://docs.microsoft.com/en-us/microsoft-365/admin/misc/password-policy-recommendations#some-common-approaches-and-their-negative-impacts
  # - https://www.ftc.gov/news-events/blogs/techftc/2016/03/time-rethink-mandatory-password-changes
  def password_complexity
    return unless require_password?

    count = 0
    count += 1 if /[A-Z]/.match? password # upper
    count += 1 if /[a-z]/.match? password # lower
    count += 1 if /[0-9]/.match? password # number
    count += 1 if /[^A-Za-z0-9]/.match? password # other (symbol, punctuation, math, etc.)
    return if count >= 3

    errors.add :password, :uniform
  end

end
