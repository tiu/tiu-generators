# frozen_string_literal: true

class PasswordsController < ApplicationController
  before_action :set_user, only: [:edit, :update]
  skip_authorization_check

  def new
    login_params = params[:login] || current_user&.email
    @password_request = PasswordChangeRequest.new(login: login_params)
  end

  def create
    @password_request = PasswordChangeRequest.new(password_change_request_params)

    if @password_request.valid?
      EmailPasswordResetJob.perform_later(@password_request.user)
      render :create
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit; end

  def update
    @user.attributes = password_change_params
    @user.valid?
    @user.errors.add(:password, 'was not provided') unless @user.crypted_password_changed?

    # filter errors
    irrelevant_keys = @user.errors.attribute_names - [:password, :password_confirmation]
    irrelevant_keys.each { |k| @user.errors.delete(k) }

    if @user.errors.empty?
      @user.update_attribute :password, @user.password
      redirect_to @user, notice: t('.updated')
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private

    # Only allow a list of trusted parameters through.
    def password_change_request_params
      params.require(:password).permit(:login)
    end

    def password_change_params
      params.require(:user).permit(:password, :password_confirmation)
    end

    def set_user
      @user = User.find_using_perishable_token(params[:reset_password_token])
      return if @user

      redirect_to current_user || login_path, alert: t('passwords.edit.inactive_link_html', new_password_path: new_password_path)
    end
end
