# frozen_string_literal: true

class ApplicationController < ActionController::Base # rubocop:disable Metrics/ClassLength
  include JsonController

  before_action :set_paper_trail_whodunnit

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Ensure authorization is everywhere (https://github.com/ryanb/cancan/wiki/Ensure-Authorization)
  check_authorization

  # Don't restrict access to static pages (high_voltage)
  skip_authorization_check if: controller_name == 'pages'

  # Handles authorization errors. Notifies user why it occurred and redirects to root_url.
  rescue_from CanCan::AccessDenied do |e|
    Rails.logger.warn { "Access denied for #{current_user.to_param} to '#{e.action}' a '#{e.subject}'." }

    # Notify users with detailed explanation
    if current_user
      action = I18n.t("errors.actions.#{e.action}", default: e.action.to_s).titleize.downcase

      # Explain to users why they were denied access.
      roles = current_user.roles.present? ? current_user.roles.to_sentence : 'regular user'
      if e.subject.is_a? Class
        flash[:error] = "As a #{roles}, you are not authorized to #{action} #{e.subject.name.pluralize.titleize}."
      else
        flash[:error] = "As a #{roles}, you are not authorized to #{action} this #{e.subject.class.name.titleize}."
      end

      redirect_back_or_default_to root_url

    # Notify non-users without explanation
    else
      flash[:error] = e.to_s

      redirect_to root_url
    end
  end

  #############################################################################
  # Private Methods
  #
  private

  def store_location(path = nil)
    path ||= request.fullpath if request.get?
    return if path.blank?

    session[:return_to] = path
  end

  # Typically called to store location before showing a form to support 'Cancel' button:
  #   `before_action :store_referrer, only: [:index, :new, :edit]`
  def store_referrer
    return if request.referer == login_url # Avoid returning to /login after the next action.

    store_location request.referer # remember where we were
  end

  def clear_location
    session[:return_to] = nil
  end

  def redirect_back_or_default_to(default)
    redirect_to url_from(params[:redirect_uri]) || url_from(session[:return_to]) || default
    session[:return_to] = nil
  end

  def current_user_session
    return @current_user_session if defined?(@current_user_session)

    @current_user_session = UserSession.find
  end

  def current_user
    return @current_user if defined?(@current_user)

    @current_user = current_user_session&.user
  end
  helper_method :current_user

  # Provides session to use when defining Abilities. Override's Cancancan default.
  def current_ability
    @current_ability ||= Ability.new(current_user, session)
  end

  def su_user
    return @su_user if defined?(@su_user)

    @su_user = User.find(session[:su_user]) if session[:su_user]
  end
  helper_method :su_user

  # Restricts action to authenticated sessions. Helpfully remembers target url
  # while a user logs in.
  def require_user
    return if current_user

    respond_to do |format|
      format.json { head :unauthorized }
      format.any {
        flash[:warning] = t('app.messages.require_user')
        redirect_to login_url(redirect_uri: (request.fullpath if request.get?))
      }
    end
  end

  def require_no_user
    return unless current_user

    flash[:warning] = t('app.messages.require_no_user')
    redirect_back_or_default_to root_url
  end

  # Sets the filename header using the name from action_filename.
  #
  # Provide a name or individual parts for custom behavior. For example,
  # override the :index action with a more helpful :action_part.
  def set_filename(name = nil, action_part: nil, with_namespace: true)
    action_filename(name, action_part: action_part, with_namespace: with_namespace)
    headers["Content-Disposition"] = "attachment; filename=\"#{@action_filename}\""
  end

  # Calculate appropriate filename for the current action:
  #   `controller_path` + `action_name` + Time.current + request.format
  #   => "Reports Outreach Events Index 2015-10-20-1117am.xls"
  #
  # Provide a name or individual parts for custom behavior. For example,
  # override the :index action with a more helpful :action_part.
  def action_filename(name = nil, action_part: nil, with_namespace: true, format: request.format.symbol)
    # return and save for use in the view. memoized.
    @action_filename ||= begin
      # Calculate name unless one is provided
      name ||= begin
        controller_part = with_namespace ? controller_path : controller_name
        controller_part = controller_part.parameterize.titleize # reports/outreach_events => 'Reports Outreach Events'
        action_part ||= action_name.titleize                    # index => 'Index'
        timestamp_part = Time.current.strftime('%F-%I%M%P')     # '2015-10-20-1117am'
        [controller_part, action_part, timestamp_part].compact_blank.join(' ')
      end

      "#{name}.#{format}"
    end
  end
  helper_method :action_filename

  def environment_info
    @environment_info ||= GetEnvironmentInfo.call
  end
  helper_method :environment_info

  # A default implementation for the destroy action.
  def destroy_record(record, redirect_path: nil)
    if record.destroy
      flash[:notice] = "#{record.model_name.human} was successfully deleted."
    else
      flash[:error] = record.errors.full_messages.to_sentence
    end

    redirect_back_or_default_to(redirect_path || url_for(record.class))
  end

  # For Will Paginate in controllers. Protects against invalid page parameters
  def page_params
    page = params[:page]&.to_i
    page if page&.positive?
  end
end
