# frozen_string_literal: true

# Paper Trail versions
class VersionsController < ApplicationController
  before_action :require_user
  load_and_authorize_resource class: 'PaperTrail::Version'

  # GET /versions
  def index
    @search = VersionSearchForm.new(version_search_form_params)
    @versions = @search.apply(versions: @versions) if @search.valid?

    # TODO: exception when linking to a deleted model
    respond_to do |format|
      format.html { # index.html.erb
        @versions = @versions.sorted.page(page_params)
      }
      format.json {
        render json: @versions
      }
      format.xlsx { # index.xlsx.erb
        set_filename
      }
    end
  end

  # GET /versions/1
  def show
    respond_to do |format|
      format.json {
        render json: @version
      }
    end
  end

  def revert
    if @version.reify
      @version.reify.save! # revert edited object
    else
      @version.item.destroy # destroy created object
    end
    redirect_back fallback_location: versions_url, notice: "Undid #{@version.event}. #{redo_link}"
  end

  private

    def version_search_form_params
      params[:q]&.permit(:author_id, :from, :to, :item_type, :item)
    end
    helper_method :version_search_form_params

    def redo_link
      link_name = params[:redo] == "true" ? "undo" : "redo"
      view_context.link_to view_context.icon('fas', 'redo', link_name.titleize),
                           revert_version_path(@version.next, redo: !params[:redo]),
                           method: :post
    end
end
