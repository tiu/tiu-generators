# Stuff for Fast JSON API serializer
#
# https://github.com/Netflix/fast_jsonapi
# https://jsonapi.org/format/
module JsonController

  private

    # Options for the Fast JSON API serializer.
    # See (https://github.com/Netflix/fast_jsonapi#collection-serialization)
    def serializer_options(collection)
      if collection.is_a? ApplicationRecord
        record_serializer_options collection
      else
        collection_serializer_options collection
      end
    end

    # Options for the Fast JSON API serializer when we have a single record.
    def record_serializer_options(record)
      { links: { self: url_for(record) } }.merge requested_serializer_options
    end

    # Options for the Fast JSON API serializer when we have a collection of records.
    def collection_serializer_options(collection)
      {
        meta: { total_pages: collection.total_pages,
                total_entries: collection.total_entries,
                per_page: collection.per_page },
        links: { prev: collection.previous_page && url_for(page: collection.previous_page),
                 self: url_for(page: collection.current_page),
                 next: collection.next_page && url_for(page: collection.next_page) }
      }.merge requested_serializer_options
    end

    # Options for the Fast JSON API serializer from the request params
    def requested_serializer_options
      { include: include_params }
    end

    def include_params
      params.slice(:include).permit(include: [])[:include]
    end

    # Consider: https://github.com/Netflix/fast_jsonapi/issues/53
    def handle_json_errors
      yield
    rescue ActionController::UnpermittedParameters => e
      # Format: https://jsonapi.org/format/#errors-processing
      render json: { errors: [{ title: e.message, status: 400.to_s }] }, status: :bad_request
    end
end
