# frozen_string_literal: true

class UserMailer < ApplicationMailer
  # after_action :prevent_delivery_when_no_recipients, only: [:example_mailer]

  def welcome(user)
    Rails.logger.info { "Emailing new user welcome to #{user}" }
    @user = user
    analytics_url_params

    mail(to: user.full_email, subject: "Welcome to #{I18n.t('app.title')}")
  end

  def reset_password_instructions(user)
    Rails.logger.info { "Emailing password update link to #{user}" }
    @user = user
    analytics_url_params(utm_source: :password_reset)

    mail(to: user.full_email, subject: "#{I18n.t('app.title')} Password Update Link")
  end

  def test_email(to:, subject: 'Test Subject', body: 'Test Body')
    raise ArgumentError, ':to address is required' unless to

    mail(to: to, subject: subject, body: body)
  end
end
