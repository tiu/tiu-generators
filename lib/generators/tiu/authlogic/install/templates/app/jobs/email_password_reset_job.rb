# frozen_string_literal: true

class EmailPasswordResetJob < EmailJob

  def perform(user)
    user.reset_perishable_token! # invalidate any previous emailed password links
    UserMailer.reset_password_instructions(user).deliver_now
  end
end
