# frozen_string_literal: true

class UserSerializer < BaseSerializer
  include FastJsonapi::ObjectSerializer
  attributes :email, :name, :first_name, :middle_name, :last_name, :status, :roles, :username,
             :login_count, :created_at, :updated_at

  # attribute(:address) { |record| record.address&.to_s }
  # attribute(:roles) { |record| record.roles.map(&:to_s) }
  # has_many :phones

  link(:self) { |user| url_helpers.user_url(user) }
end
