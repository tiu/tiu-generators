# frozen_string_literal: true

class BaseSerializer
  class << self
    # Allows using helpers like `url_helpers.user_path(user)`.
    # Requires setting up default_url_options in each environment.
    delegate :url_helpers, to: :'Rails.application.routes'

    # Allows using helpers like `helpers.dom_id(user)`.
    delegate :helpers, to: :'ActionController::Base'
  end
end
