# frozen_string_literal: true

class PasswordChangeRequest
  include ActiveModel::Model
  include ActiveModel::Attributes

  delegate :helpers, to: 'ActionController::Base'

  attribute :login, :string
  attribute :user
  attribute :support_email, :string, default: I18n.t('app.support_email')

  #
  # Validations
  #

  validates :login, presence: true
  validate :user_found
  validate :user_no_problems

  #
  # Methods
  #

  def model_name
    ActiveModel::Name.new PasswordChangeRequest, nil, 'Password'
  end

  def login=(value)
    self.user = User.find_by_username_or_email(value)
    super
  end

  def login?
    login.present?
  end

  private

    def user_found
      return unless login? && user.blank?

      # Lookup :message and apply :sanitize since the _html suffix isn't respected as with `I18n.t`.
      message = errors.generate_message(:base, :account_not_found_html, email: support_email)
      errors.add :base, helpers.sanitize(message)
    end

    def user_no_problems
      return unless user.present? && user_has_problems?

      # Lookup :message and apply :sanitize since the _html suffix isn't respected as with `I18n.t`.
      message = errors.generate_message(:base, :account_problem_html, email: support_email)
      errors.add :base, helpers.sanitize(message)
    end

    def user_has_problems?
      user && !(user.email? && user.active?)
    end
end
