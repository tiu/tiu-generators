# frozen_string_literal: true

class EmailWelcomeUserJob < EmailJob

  def perform(user)
    UserMailer.welcome(user).deliver_now
  end
end
