# frozen_string_literal: true

module SessionHelper
  # Login as the given user. Asserts successful login message.
  # TODO: this way is used in Functional Tests. Can we share it with System tests?
  def login(user)
    post user_session_url, params: { user_session: { login: user.username, password: user.password } }
    assert_equal I18n.t('user_sessions.create.welcome'), flash[:notice]
  end

  # Login as the given user. Asserts successful login message.
  def login_as(user)
    visit login_url

    assert_selector 'h2', text: 'Login'
    fill_in 'Login', with: user.username
    fill_in 'Password', with: user.password

    click_button 'Log In'

    assert_text I18n.t('user_sessions.create.welcome')
  end

  # Logout of current session
  def logout
    visit logout_url
    assert_text I18n.t('user_sessions.destroy.logout')
  end

  # Wrap login for given user around block
  def as_user(user)
    assert_not_nil user
    login_as(user)
    yield
    logout
  end
end
