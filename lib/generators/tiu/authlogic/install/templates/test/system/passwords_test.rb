require "application_system_test_case"

class PasswordsTest < ApplicationSystemTestCase
  test "request a Password Change" do
    user = FactoryBot.create :user

    visit login_url
    click_link 'Forgot your password?'

    assert_selector "h2", text: "Change Your Password?"
    fill_in 'Login', with: user.username

    click_button I18n.t('passwords.new.submit')

    assert_selector 'p', text: I18n.t('passwords.create.sent_email')
  end

  test "complete a Password Change" do
    user = FactoryBot.create :user
    visit edit_password_url(user.perishable_token)

    assert_selector 'h2', text: 'Change My Password'
    fill_in 'Password', with: user.password
    fill_in 'Confirm new password', with: user.password_confirmation

    click_button 'Change my password and log me in'

    assert_selector 'p.flash-message.alert-info', text: 'Password successfully updated'
  end

  test "complete a Password Change while logged in" do
    user = FactoryBot.create :user

    visit login_url
    fill_in 'Login', with: user.username
    fill_in 'Password', with: user.password
    click_button 'Log In'
    user.reload

    visit edit_password_url(user.perishable_token)

    assert_selector 'h2', text: 'Change My Password'
    fill_in 'Password', with: user.password
    fill_in 'Confirm new password', with: user.password_confirmation

    click_button 'Change my password and log me in'

    assert_selector 'p.flash-message.alert-info', text: 'Password successfully updated'

    assert_not_equal user.perishable_token, user.reload.perishable_token, "Perishable token should reset with a password update."
  end

  test "attempt a Password Change with an expired link" do
    user = FactoryBot.create :user

    travel User.perishable_token_valid_for + 1 # Wait until the password reset link expires
    visit edit_password_url(user.perishable_token)

    text = ApplicationController.helpers.strip_tags(I18n.t('passwords.edit.inactive_link_html'))
    assert_selector 'p.flash-message.alert-danger', text: text
  end

  test "attempt a Password Change with an old link" do
    user = FactoryBot.create :user

    visit new_password_url(login: user.username)
    click_button I18n.t('passwords.new.submit')

    # Use earlier token
    visit edit_password_url(user.perishable_token)

    text = ApplicationController.helpers.strip_tags(I18n.t('passwords.edit.inactive_link_html'))
    assert_selector 'p.flash-message.alert-danger', text: text
  end
end
