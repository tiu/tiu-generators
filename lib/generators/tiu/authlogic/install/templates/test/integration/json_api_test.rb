require 'test_helper'

class JsonApiTest < ActionDispatch::IntegrationTest
  test "unauthenticated JSON requests return 401 Unauthorized" do
    get users_url, as: :json
    assert_response :unauthorized
  end
end
