require 'js_helper'
require 'test_helper'

# Requires you have installed chromedriver, `brew cask install chromedriver`
class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  include JsHelper

  # Note, for debugging it is helpful to see the browser with:
  # driven_by :selenium, using: :chrome, screen_size: [1200, 1200]
  driven_by :selenium, using: :headless_chrome, screen_size: [1400, 1400]

  teardown do
    check_for_javascript_errors
  end
end
