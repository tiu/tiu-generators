module JsHelper
  def check_for_javascript_errors
    # Skip check with old Selenium and Chrome 76+
    # - https://rubygems.org/gems/selenium-webdriver/versions/3.142.4
    # - https://github.com/SeleniumHQ/selenium/commit/5fe20e7658148a4fff90f612556853746bca1cad
    # - https://github.com/SeleniumHQ/selenium/issues/7390
    # - https://github.com/SeleniumHQ/selenium/commit/26d8b67a58e99fa4121376537ea434c761eea5e6
    if Gem.loaded_specs['selenium-webdriver'].version < Gem::Version.new('4.0.0')
      warn "Skipping #{__method__} to workaround #7390. Upgrade to 4.0.0"
      return
    end

    return if console_logs.blank?

    log_messages = console_logs.map { |msg| "* #{msg}" }.join("\n")

    flunk <<~MESSAGE.strip
      Javascript Console Errors:
      #{log_messages}
    MESSAGE
  end

  private

    # NOTE: Ignore 422 errors since form validation responses are expected.
    def console_logs
      @console_logs ||= page.driver.browser.logs.get(:browser)
                            .reject { |log_entry| log_entry.message.ends_with? "422 (Unprocessable Entity)" }
    end
end
