require 'simplecov' # Load SimpleCov. Configure in .simplecov

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require "authlogic/test_case"
require 'session_helper'

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  include SessionHelper

  # https://www.rubydoc.info/github/binarylogic/authlogic/Authlogic/TestCase
  # https://github.com/binarylogic/authlogic/blob/master/lib/authlogic/test_case.rb
  include Authlogic::TestCase
  setup :activate_authlogic
end
