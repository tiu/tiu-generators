require 'test_helper'

class UserSessionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = FactoryBot.create :user
    @sysadmin = FactoryBot.create :sysadmin
  end

  # GET /users/sessions/login
  # GET /login
  test "should get new" do
    get new_user_session_url
    assert_response :success

    get login_url
    assert_response :success
  end

  # POST /users/session
  test "should create user_session" do
    post user_session_url, params: { user_session: { login: @user.username, password: @user.password } }

    assert_redirected_to root_url
    assert_equal I18n.t('user_sessions.create.welcome'), flash[:notice]
  end

  # GET /logout
  # DELETE /users/session
  test "should destroy user_session" do
    login(@user)

    delete user_session_url

    assert_nil session[:su_user]
    assert_nil flash[:warning]
    assert_equal I18n.t('user_sessions.destroy.logout'), flash[:notice]
    assert_redirected_to login_url
  end

  # GET /su/:id
  test "should switch users" do
    login(@sysadmin)
    get su_url(@user)

    assert_equal @sysadmin.id, session[:su_user]
    assert_equal I18n.t('user_sessions.su.success', user: @user), flash[:success]
  end

  test "should unswitch users" do
    login(@sysadmin)
    get su_url(@user)
    get unsu_url

    assert_nil session[:su_user]
    assert_equal I18n.t('user_sessions.unsu.success', user: @sysadmin), flash[:success]
  end

  test "should timeout" do
    login(@user)
    get seconds_remaining_user_session_url
    assert_predicate @response.body.to_f, :positive?

    travel User.logged_in_timeout.seconds + 1.second
    get seconds_remaining_user_session_url
    assert_predicate @response.body.to_f, :negative?

    get account_url
    assert_redirected_to login_url(redirect_uri: account_path)
  end
end
