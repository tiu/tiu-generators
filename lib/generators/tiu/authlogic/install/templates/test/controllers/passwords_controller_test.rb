require 'test_helper'

class PasswordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = FactoryBot.create :user
  end

  test "should get new" do
    get new_password_url
    assert_response :success
  end

  test "should create password reset email" do
    assert_enqueued_with(job: EmailPasswordResetJob) do
      post passwords_url, params: { password: { login: @user.username } }
    end

    assert_redirected_to login_url
    assert_equal I18n.t('passwords.create.sent_email'), flash[:success]
  end

  test "should create password reset email and logout" do
    login(@user)
    assert_enqueued_with(job: EmailPasswordResetJob) do
      post passwords_url, params: { password: { login: @user.username } }
    end

    assert_redirected_to logout_url
    assert_equal I18n.t('passwords.create.sent_email'), flash[:success]
    # assert_equal I18n.t('user_sessions.destroy.logout'), flash[:notice]
  end

  test "should get edit" do
    get edit_password_url(@user.perishable_token)
    assert_response :success
  end

  test "should update password" do
    password = @user.password
    patch password_url(@user.perishable_token),
          params: { user: { password: password, password_confirmation: password } }
    assert_equal I18n.t('passwords.update.updated'), flash[:notice]
    assert_redirected_to user_path(@user)
  end

end
