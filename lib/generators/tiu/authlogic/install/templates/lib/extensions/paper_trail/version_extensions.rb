# frozen_string_literal: true

module PaperTrail
  module VersionExtensions

    def self.prepended(base)
      base.class_eval do
        scope :sorted, -> { reorder(created_at: :desc) }
        scope :recent, -> { sorted.first(3) }
      end
    end

    #
    # Methods
    #

    # Get User when whodunnit is an id, otherwise return the raw string.
    # e.g.
    # => #<User: id: 1, email: 'ahoyt@tiu11.org'...>
    # => 'ahoyt: console'
    # => 'ahoyt: rake db:import:users'
    # => 'Anonymous'
    def who
      @who ||= begin
        user_id = whodunnit.to_i
        if user_id.positive?
          User.find_by(id: user_id) || user_id
        else
          whodunnit || 'Anonymous'
        end
      end
    end

    # Returns model's class object when available, +nil+ if a class was renamed or removed.
    def item_class
      @item_class ||= item_type.safe_constantize
    end

    # Enhance default PaperTrail changeset.
    # - replace :belongs_to foreign keys with the associated record
    # - on :destroy, mimic all attributes being deleted
    # - etc.
    def enhanced_changeset
      result_changeset =
        if item_class.nil? # TODO: seems a lot like 'destroy' case. Combinable? When does this occur?
          object_changes
        elsif event == 'destroy' # empty changeset on 'delete', but we'll mimic all attributes being cleared
          object.compact_blank.transform_values { |v| [v, nil] }
        else # nil or Hash
          changeset.to_h.deep_dup
        end

      expand_belongs_to_associations(result_changeset)

      result_changeset
    end

    # Consistent place to get the object attributes, since #object is nil on create.
    def object_attributes
      object || begin
        h = {}
        object_changes.each { |key, change| h[key] = change.last }
        return h
      end
    end

    # TODO: Determine if there should be a parent object instance when polymorphic. What is this returning?
    def object_instance
      @object_instance ||= if parent_association.present?
                             parent_model
                           else
                             item_class&.find_by(id: item_id)
                           end
    end

    # based on notes from (http://coryforsyth.com/2013/06/02/programmatically-list-routespaths-from-inside-your-rails-app/)
    def route?
      routes.present?
    end

    def object_path(only_path: true)
      route = routes.first # TODO: ignoring that we might have multiple routes to pick from
      return unless route

      params = { only_path: }
               .merge(route.defaults) # ex. { controller: 'users', action: 'show' }
               .merge(object_instance.slice(*route.required_parts).symbolize_keys)
      ::Rails.application.routes.url_for(params)
    end

    def object_url(only_path: false)
      object_path(only_path:)
    end

    # All :show routes on the object's controller
    def routes
      return unless object_instance

      all_routes = ::Rails.application.routes.routes
      controller_name = object_instance.model_name.route_key

      all_routes.select do |route|
        route.defaults == { controller: controller_name, action: 'show' }
      end
    end

    def dom_id
      cache_key.dasherize.parameterize
    end

    private

      def belongs_to_associations
        @belongs_to_associations ||= item_class&.reflect_on_all_associations(:belongs_to) || []
      end

      # Replace any :belongs_to foreign keys with the associated record
      # TODO: handle polymorphic. When you handle poloymorphic, be aware of belongs_to parent on assignment
      # TODO: combine `klass.find_by` calls into one for performance
      def expand_belongs_to_associations(result_changeset)
        belongs_to_associations.select { |a| a.foreign_key.in?(result_changeset.keys) && !a.polymorphic? }.each do |a|
          before_id, after_id = result_changeset[a.foreign_key]
          before_object = before_id && a.klass.find_by(id: before_id)
          after_object = after_id && a.klass.find_by(id: after_id)

          if before_object || after_object # don't replace keys that we couldn't look up
            result_changeset.delete(a.foreign_key) unless result_changeset.empty?
            result_changeset[a.name.to_s] = [before_object, after_object]
          end
        end
      end

      # Assumes the polymorphic is a parent record and there is only one
      def parent_association
        @parent_association ||= belongs_to_associations.find do |a|
          a.polymorphic? && a.active_record.name.eql?(item_type) # TODO: why the name check?
        end
      end

      def parent_model
        return if parent_association.blank?

        model_class = object_attributes["#{parent_association.name}_type"]&.safe_constantize
        foreign_key = "#{parent_association.name}_id"
        model_class.find_by(id: object_attributes[foreign_key]) if model_class && foreign_key
      end

  end
end
