# frozen_string_literal: true

module Type

  # Convert role String or Symbol to Role object.
  #
  class Role < ActiveRecord::Type::String

    def serialize(value)
      value = cast_value(value) unless value.is_a? ::Role
      value.name if value.is_a? ::Role
    end

    private

      def cast_value(value)
        return if value.blank?

        case value
        when ::String, ::Symbol
          ::Role[value] || value
        when ::Role
          value
        end
      end

  end

end
