# Record blame for versions initiated from the console and rake. Still uses current_user for web changes.
if defined?(Rake.application)
  PaperTrail.request.whodunnit = "#{`whoami`.strip}: #{Rake.application.name} #{Rake.application.top_level_tasks.join(' ')}"
elsif defined?(::Rails::Console)
  PaperTrail.request.whodunnit = "#{`whoami`.strip}: rails console"
end
