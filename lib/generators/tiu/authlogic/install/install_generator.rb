# frozen_string_literal: true

require 'helpers/generator_helper'

module Tiu
  module Authlogic
    # Scaffolds users with session management, password reset, change history, and tests.
    class InstallGenerator < Rails::Generators::Base
      include ::Helpers::GeneratorHelper
      source_root File.expand_path('templates', __dir__)
      argument :attributes,
               type: :array,
               default: %w[email:string first_name:string middle_name:string last_name:string status:integer],
               banner: 'field:type field:type'

      # Every method that is declared below will be automatically executed when the generator is run

      # Verify dependencies specific to this generator, since they are not
      # listed as dependencies in the gemspec.
      def check_dependencies
        verify_gem 'authlogic', '>= 5.0'
        verify_gem 'cancancan', '>= 1.0'
        verify_gem 'friendly_id', '>= 5'
        verify_gem 'paper_trail', '>= 12.0'

        if gem_installed? 'webpacker'
          verify_gem 'webpacker', '>= 5.4'
        else
          verify_gem 'jsbundling-rails', '~> 1.0'
        end
      end

      def setup_paper_trail
        generate 'paper_trail:install --with-changes'

        # Use Postgresql :jsonb instead of :text for :object and :object_changes.
        # See https://github.com/paper-trail-gem/paper_trail#postgresql-json-column-type-support

        migration_file = Dir['db/migrate/*_create_versions.rb'].first
        gsub_file migration_file, 't.text     :object, limit: TEXT_BYTES', 't.jsonb    :object'

        migration_file = Dir['db/migrate/*_add_object_changes_to_versions.rb'].first
        gsub_file migration_file, ':object_changes, :text, limit: TEXT_BYTES', ':object_changes, :jsonb'
      end

      def create_users
        generate "scaffold user #{attributes.join(' ')} --no-stylesheets --no-javascripts --no-helper --force"
      end

      def copy_templates
        directory 'app'
        directory 'config'
        directory 'jobs'
        directory 'lib'
        directory 'spec'
        directory 'test'
      end

      def load_spec_support_directory
        # Commented by default. Format varies some with spotty Rubocop fixes.
        uncomment_lines 'spec/rails_helper.rb', /Rails.root.join\W+spec\W+support/
      end

      def update_files
        route read_template('routes.rb.delta')
        # TODO: consider moving modal data attributes to meta tags,
        # inspired by javan/Basecamp building a model from these meta values
        # insert_into_file 'app/views/layouts/application.html.erb',
        #                  "  <%= tag.meta name: 'session-expires-at', content: current_user.session_expires_at.iso8601 if current_user %>\n",
        #                  after: "<%= meta_robots %>\n"
        insert_into_file 'app/views/layouts/application.html.erb',
                         "    <%= render 'session_timeout' %>\n",
                         after: "<%= render 'flash_messages' %>\n"
        insert_into_file Dir['db/migrate/*_create_users.rb'].first,
                         read_template('create_users.rb.delta'),
                         before: "\n      t.timestamps"
        insert_into_file 'app/views/application/_nav.html.erb',
                         "        <%= render 'login_menu' %>\n",
                         before: '      </div><!--/.nav-collapse -->'
        insert_into_file 'config/initializers/extensions.rb',
                         'PaperTrail::Version.prepend PaperTrail::VersionExtensions'
      end

      def register_types
        append_to_file 'config/initializers/types.rb', <<~CONFIG
          ActiveRecord::Type.register(:role, Type::Role)
          ActiveModel::Type.register(:role, Type::Role)
        CONFIG
      end

      def install_packages
        say "Installing JavaScript dependencies"
        run "yarn add luxon@^1.25.0"
      end

      def remove_legacy_packages
        remove_file 'vendor/assets/javascripts/sprintf.js'
        run "yarn remove sprintf-js"
      end
    end
  end
end
