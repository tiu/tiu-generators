# frozen_string_literal: true

# Overrides https://github.com/rails/rails/blob/main/railties/lib/rails/generators/erb/scaffold/scaffold_generator.rb
#
# See
# * https://stackoverflow.com/questions/4696954/how-to-have-the-scaffold-to-generate-another-partial-view-template-file
# * https://stackoverflow.com/questions/16320882/rails-generate-both-html-and-js-views-with-scaffold
# * https://www.softwaremaniacs.net/2014/01/replacing-scaffoldcontroller-generator.html
require "rails/generators/erb"
require "rails/generators/resource_helpers"
require 'rails/generators/erb/scaffold/scaffold_generator'
require 'helpers/generator_helper'

module Erb # :nodoc:
  module Generators # :nodoc:
    class ScaffoldGenerator < Base # :nodoc:
      include Rails::Generators::ResourceHelpers
      include ::Helpers::GeneratorHelper

      source_paths << File.expand_path('templates', __dir__)

      argument :attributes, type: :array, default: [], banner: "field:type field:type"

      def create_root_folder
        empty_directory File.join("app/views", controller_file_path)
      end

      def copy_view_files
        available_views.each do |view|
          formats.each do |format|
            filename = filename_with_extensions(view, format)
            template filename, File.join("app/views", controller_file_path, filename)
          end
        end

        template "partial.html.erb", File.join("app/views", controller_file_path, "_#{singular_name}.html.erb")

        # TODO: Extract to its own generator, like jbuilder:
        # https://github.com/rails/jbuilder/blob/main/lib/generators/rails/jbuilder_generator.rb
        filename = gem_installed?('caxlsx_rails') ? 'index.xlsx.axlsx' : 'index.xls.erb'
        template filename, File.join("app/views", controller_file_path, filename)
      end

    private

      def excel_format
        gem_installed?('caxlsx_rails') ? :xlsx : :xls
      end

      def available_views
        %w(index edit show new _form _action_menu _toolbar)
      end
    end
  end
end
