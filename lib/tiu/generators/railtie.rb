# frozen_string_literal: true

module Tiu
  module Generators
    # Resources to understand railties:
    # * https://edgeapi.rubyonrails.org/classes/Rails/Railtie.html
    # * https://api.rubyonrails.org/classes/Rails/Railtie/Configuration.html#method-i-app_generators
    class Railtie < Rails::Railtie
      # Override Rails Scaffolding Generator
      generators do
        require_relative '../../generators/erb/scaffold/scaffold_generator'
      end

      # Make generator template overrides available to the application
      config.app_generators.templates << File.expand_path('../../templates', __dir__)
    end
  end
end
