# frozen_string_literal: true

require 'tiu/generators/version'
require 'tiu/generators/railtie' if defined?(Rails)

module Tiu
  module Generators
    class Error < StandardError; end
    # Your code goes here...
  end
end
