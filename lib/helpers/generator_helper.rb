# frozen_string_literal: true

module Helpers
  # Helper methods used in TIU Rails Generators
  module GeneratorHelper
    class << self
    end

    # Read a template file from the generator's templates folder
    def read_template(path)
      path = File.join(self.class.source_root, path)
      File.open(path).read
    end

    def verify_gem(gem_name, version = nil)
      abort "Dependency #{[gem_name, version].join(' ')} is not installed!".red unless gem_installed? gem_name, version
    end

    def gem_installed?(gem_name, version = nil)
      if version
        `gem list -ie #{gem_name} -v '#{version}'`.strip == 'true'
      else
        `gem list -ie #{gem_name}`.strip == 'true'
      end
    end
  end
end
