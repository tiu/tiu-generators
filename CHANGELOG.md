# Changelog

Some notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project somewhat adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Replace Twitter Bootstrap 3 with 4.
- Upstream improvements from PDT project.
- Add tiu:model:address generator.
- Add tiu:model:link generator.
- Add tiu:model:phone generator.
- Add tiu:active_job:install generator.
- Add tiu:pg_search:install generator.
- Add tiu:app:setup generator.

### Changed

- Moved PaperTrail::Version class overrides to a module.
  ```
  rm app/models/paper_trail/version.rb && rails g tiu:authlogic:install
  ```
- Forward-ports Rails 7 scaffold_controller template (#41026)[https://github.com/rails/rails/pull/41026]. Update your existing controllers to use 422 instead of 200 on form error responses. Use this replacement in your favorite editor:
  ```
  rg '(render :(new|edit))$' --replace '$1, status: :unprocessable_entity' app/controllers/*.rb
  ```
- Refactored (and fixed) scaffold generator. No longer requires hack of namespacing :all and making that fallback to :erb. Uses a railtie to override ActiveRecord's default model.rb.tt, which is necessary now that we're in a gem.
- Refactored `tiu:authlogic:install` generator. Before re-applying, rename your existing files with:
  ```
  mv app/controllers/password{_resets,s}_controller.rb
  mv app/forms/password_{reset,change_request}.rb
  mv app/views/password{_resets,s}
  mv app/views/user_mailer/{password_reset,reset_password_instructions}.html.erb
  mv app/views/user_mailer/{password_reset,reset_password_instructions}.text.erb
  mv test/system/password{_resets,s}_test.rb
  ```

### Removed

- None

## 1.0.0 - 2019-04-30

### Added

- Extracted Rails generators from https://bitbucket.org/tiu/rails-application-template/
- Moved `angularjs:install` and `authlogic:install` under the `tiu` namespace.

### Changed

- None

### Removed

- None
