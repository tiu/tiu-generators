
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "tiu/generators/version"

Gem::Specification.new do |spec|
  spec.name          = "tiu-generators"
  spec.version       = Tiu::Generators::VERSION
  spec.authors       = ["Anson Hoyt"]
  spec.email         = ["ahoyt@tiu11.org"]

  spec.summary       = %q{TIU's generators for Rails applications}
  spec.description   = %q{A bundle of simple Rails generators for scaffolding, etc.}
  spec.homepage      = "https://bitbucket.org/tiu/tiu-generators"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

    spec.metadata["homepage_uri"] = spec.homepage
    spec.metadata["source_code_uri"] = "https://bitbucket.org/tiu/tiu-generators"
    spec.metadata["changelog_uri"] = "https://bitbucket.org/tiu/tiu-generators/CHANGELOG.md"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.required_ruby_version = ">= 2.7.8"

  spec.add_dependency "activerecord", [">= 4.2", "< 7.2"]
  spec.add_dependency "bootstrap", "~> 4.0"
  spec.add_dependency "font-awesome-sass", ">= 5.0"
  spec.add_dependency "simplecov"

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", ">= 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
end
