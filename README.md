# TIU Generators

A collection of Rails generators used across many of TIU's Rails applications. Used in conjunction with the [TIU Rails Application Template](https://bitbucket.org/tiu/rails-application-template/).

**Version Compatibility**

| version | branch   | ruby     | bootstrap |
|--------:|----------|----------|-----------|
|       1 | 1-stable | >= 2.3.0 | ~> 3.0    |
|       2 | master   | >= 2.3.0 | ~> 4.0    |

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tiu-generators', git: 'git@bitbucket.org:tiu/tiu-generators.git', branch: '1-stable'
# or
gem 'tiu-generators', bitbucket: 'tiu/tiu-generators', branch: '1-stable'
```

And then execute:

    $ bundle

If your application applied the [TIU Rails Application Template](https://bitbucket.org/tiu/rails-application-template/) prior to 4.0, then clean out the scaffolding and templates that are now in this gem:

    rm -rfv ./lib/generators/{all,angularjs,authlogic,tiu}
    rm -rfv ./lib/templates/{active_record,erb}
    rm ./lib/helpers/generator_helper.rb

If your app is still on Font Awesome 4, use the [Font Awesome Migrator](https://bitbucket.org/tiu/font-awesome-migrator) to upgrade to version 5.

## Usage

    rails g scaffold
    rails g model
    rails g tiu:app:setup
    rails g tiu:active_job:install
    rails g tiu:angularjs:install
    rails g tiu:authlogic:install
    rails g tiu:install_typeahead
    rails g tiu:model:address
    rails g tiu:model:link
    rails g tiu:model:phone
    rails g tiu:pg_search:install

See the guide to learn about [Rails Generators & Templates](https://guides.rubyonrails.org/generators.html)

## TODO

Things we're thinking about adding:

* Consider email verification, like devise "confirmable"
  https://devhints.io/devise
  https://github.com/heartcombo/devise
* Generator for form_object, service, web_service
* Add Serializer generator to scaffolding?
* A controller generator with our typical tweaks.
  Consider ideas from [draper's controller_override](https://github.com/drapergem/draper/blob/master/lib/generators/controller_override.rb)
* Generators for commonly recycled models: Address, Link, Organization, Phone
* Generators for commonly recycled types: :editor_text, :link, :token, :phone_number, :state_code
* Consider [thoughtbot/suspenders](https://github.com/thoughtbot/suspenders/tree/master/lib/suspenders/generators)

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

We're happy to get contributions. Bug reports are helpful. Pull-requests (that solve your bug) are much more interesting, and give us more incentive to take a look.

Please understand, we're a small team supporting a large number of projects, so we can't always give your problems or good ideas the attention we would like. Outside contributions are accepted on a best-effort basis. Usually, we take a look when we're in the codebase for our own needs. That can be weeks, months...sometimes longer.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
